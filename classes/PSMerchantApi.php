<?php
/**
 *  Shopacitor Merchant API Prestashop extension
 * 
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

require_once(dirname(__FILE__).'/../api/MerchantApi.php');
include_once(_PS_MODULE_DIR_.'shopacitor/shopacitor.php');

use \Shopacitor\HTTP;
use \Shopacitor\Model;
use \Shopacitor\Credentials;
use \Shopacitor\HTTP\StatusCode;

class ApiCartController extends CartControllerCore
{
	public function updateProduct($id_product, $id_product_attribute = 0, $qty = 0)
	{
		if ($qty == 0)
			return;
	
		$this->id_product = $id_product;
		$this->id_product_attribute = $id_product_attribute;
		$this->qty = abs($qty);		
		$_GET['add'] = true;
		$_GET['op'] = $qty > 0 ? 'up' : 'down';
		
		$this->processChangeProductInCart();

		if (count($this->errors))
			throw new Exception(implode("\n", $this->errors), StatusCode::BAD_REQUEST);
	}

	public function deleteProduct($id_product, $id_product_attribute = 0)
	{
		$this->id_product = $id_product;
		$this->id_product_attribute = $id_product_attribute;		
		$this->processDeleteProductInCart();

		if (count($this->errors))
			throw new Exception(implode("\n", $this->errors), StatusCode::BAD_REQUEST);
	}
	
	public function getErrors()
	{
		return $this->errors;
	}
	
	public function getCart()
	{
		return $this->context->cart;
	}
}


class PSMerchantApi extends ShopacitorMerchantApi
{
	public function init()
	{
		parent::init();
		$this->powered_by .= ' (Prestashop '._PS_VERSION_.')';
		return $this;
	}

	private function getPSCustomer()
	{
		$credential = $this->getCustomerCredentials();
		$ps_customer = new Customer();

		if (
		    $credential &&
		    $credential->password &&
		    Validate::isPasswd($credential->password) &&
		    $credential->username &&
		    Validate::isEmail($credential->username)
		)
			$ps_customer = $ps_customer->getByEmail($credential->username, $credential->password);

		if (!$ps_customer || !$ps_customer->id)
			throw new Exception(Tools::displayError('Unauthorized'), StatusCode::UNAUTHORISED);


		// Login user
		$context = Context::getContext();
		$context->cookie->id_customer = (int)($ps_customer->id);
		$context->cookie->customer_lastname = $ps_customer->lastname;
		$context->cookie->customer_firstname = $ps_customer->firstname;
		$context->cookie->logged = 1;
		$context->cookie->write();

		return $ps_customer;
	}

	/**
	 * @return ApiCartController
	 */
	private function getPSCartController()
	{
		static $controller = null;

		if (!$controller)
		{
			$controller = new ApiCartController();
			$controller->init();
		}
		return $controller;
	}

	private function getPSCart($cart_id)
	{
		return $this->getPSCartController()->getCart();
	}

	private function getPSLangID($lang = null)
	{
		$iso_code = $lang;
		$id_lang = ($iso_code && Validate::isLanguageIsoCode($iso_code)) ? (int)Language::getIdByIso($iso_code) : 0;

		if (!$id_lang)
		{
			$ps_language = Context::getContext()->language;
			$id_lang = $ps_language->id;
			$lang = $ps_language->iso_code;
		}

		return $id_lang;
	}

	public function getInfo(Model\Shop $shop)
	{
		$shop->id = 	Configuration::get('SHOPACITOR_SHOP_ID');
		$shop->name = 	Configuration::get('PS_SHOP_NAME');
		$shop->domain = Configuration::get('SHOPACITOR_SHOP_DOMAIN');
		$shop->email = 	Configuration::get('SHOPACITOR_SHOP_EMAIL');
		$shop->mode = 	Configuration::get('SHOPACITOR_SHOP_MODE');
		$shop->api_url = str_replace('%7Bresource%7D', '{resource}', Context::getContext()->link->getModuleLink('shopacitor', 'Api', array('resource'=>'{resource}')));

		return $shop;
	}

	public function getLogo(Model\Image $logo)
	{
		$logo->path = _PS_MODULE_DIR_.'shopacitor/resources/logo4shopacitor.jpg';
		return $logo;
	}

	public function getLanguages(Model\Container $languages)
	{
		foreach (Language::getLanguages() as $l)
		{
			$language = Model\Language::LanguageWithIETFTag($l['language_code'], $l['name']);
			$languages->addModel($language);
		}
		return $languages;
	}

	public function getShippingLocations(Model\Container $container)
	{
		$context = Context::getContext();
		$id_lang = $context->language->id;

		// Generate countries list
		$countries =  Configuration::get('PS_RESTRICT_DELIVERED_COUNTRIES') ? 
			Carrier::getDeliveredCountries($id_lang, true, true) :
			Country::getCountries($id_lang, true);
			
		foreach ($countries as $c)
		{
			if ((int)$c['contains_states'])
			{
				foreach ($c['states'] as $s)
					$container->addModel(Model\ShippingLocation::LocationWithISO($c['iso_code'], $s['iso_code'], $c['country'], $s['name']));
			}
			else
				$container->addModel(Model\ShippingLocation::LocationWithISO($c['iso_code'], null, $c['country']));			
		}

		return $container;
	}

	public function getProducts(Model\Container $container, $lang, $modified_since, $offset, $limit)
	{
		$id_lang = $this->getPSLangID($lang);
		$context = Context::getContext();
		$currency = Currency::getDefaultCurrency();

		// Get product by sync data
		$products = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS
		('
			SELECT SQL_CALC_FOUND_ROWS p.*,
				product_shop.*,
				pl.*,
				m.`name` AS manufacturer_name,
				UNIX_TIMESTAMP(p.`date_upd`) as `updated`,
				UNIX_TIMESTAMP(p.`date_add`) as `created`
			FROM `'._DB_PREFIX_.'product` p
			'.Shop::addSqlAssociation('product', 'p').'
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
			WHERE
				UNIX_TIMESTAMP(p.`date_upd`) > '.$modified_since.'
				AND pl.`id_lang` = '.(int)$id_lang.'
				AND product_shop.`visibility` IN ("both", "catalog")
				AND product_shop.`active` = 1
			ORDER BY p.`date_upd`
			LIMIT '.$offset.', '.$limit
		);

		$total = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->GetValue('SELECT FOUND_ROWS()');

		foreach ($products as $p)
		{
			$id_product = $p['id_product'];

			// Get tax info
			$p = Product::getTaxesInformations($p);		
			
			// Get tags
			$tags = Tag::getProductTags($id_product);

			// Get cover image
			$cover = Product::getCover($id_product, $context);

			// Create product
			$product = Model\Product::Model();
					
			$product->id = $id_product;
			$product->name = $p['name'];
			$product->ean13 = $p['ean13'];
			$product->reference = $p['reference'];
			$product->manufacturer = $p['manufacturer_name'];
			$product->short_description = $p['description_short'];
			$product->description = $p['description'];
			$product->tags = ($tags && isset($tags[$id_lang])) ? $tags[$id_lang] : null;
			$product->currency = $currency->iso_code;
			$product->cover_id = (int)$cover['id_image'];
			$product->created = (int)$p['created'];
			$product->updated = (int)$p['updated'];
			$product->price = Product::getPriceStatic($id_product, true, null, 2);		

			// Get features
			$ps_features = Product::getFrontFeaturesStatic($id_lang, $id_product);
			foreach ($ps_features as $f)
			{
				$feature = Model\ProductFeature::Model();
				$feature->name = $f['name'];
				$feature->value = $f['value'];
				$product->addFeature($feature);
			}
		
			// Add product
			$container->addModel($product);	
		}
		return $total;
	}

	public function getCategory($category_id, $lang, $products_offset, $products_limit, Model\Category $category)
	{
		$id_lang = $this->getPSLangID($lang);

		$context = Context::getContext();
   		$ps_category = $category_id ? new Category($category_id, $id_lang) : Category::getRootCategory($id_lang);
   		
   		if (!$ps_category->id)
			throw new Exception(Tools::displayError('Category not found'), StatusCode::NOT_FOUND);
   		  
   		// Create category model
		$category->id = $ps_category->id;
		$category->name = $ps_category->name;
	        
		// Add subcategories
		$ps_categories = $ps_category->getSubCategories($id_lang);
		foreach ($ps_categories as $c)
		{
			$subcategory = Model\Category::Model();
		        $subcategory->id = (int)$c['id_category'];
		        $subcategory->name = $c['name'];
			$category->addSubcategory($subcategory);
	        }


		// Get prodcuts
		$page_nr = ($products_offset / $products_limit + 1);
		$ps_products = $ps_category->getProducts($id_lang, $page_nr, $products_limit);
		$currency = Currency::getDefaultCurrency()->iso_code;

		if ($ps_products) foreach ($ps_products as $p)
		{
			$product = Model\Product::Model();
			
			$product->id = (int)$p['id_product'];
			$product->name = $p['name'];
			$product->currency = $currency;
			$product->price = (int)$p['show_price'] ? $p['price'] : null;
			
			if ($p['id_image'])
			{
				$ids = explode('-', $p['id_image']);
				$product->cover_id = (int)$ids[1];
			}
			$category->addProduct($product);		
		}

		// Get total products
		return (int)$ps_category->getProducts($id_lang, $page_nr, $products_limit, null, null, true);
	}

	public function getProduct($product_id, $lang, Model\Product $product)
	{
		$id_lang = $this->getPSLangID($lang);

		$ps_product = new Product($product_id, true, $id_lang);

		if (!$ps_product->id)
			throw new Exception(Tools::displayError('Product not found'), StatusCode::NOT_FOUND);

		$context = Context::getContext();
		$currency = Currency::getDefaultCurrency();
		$price = $ps_product->show_price ? $ps_product->getPrice(true, null, 2) : null;

		// Set product data
		$product->id = $ps_product->id;
		$product->name =  $ps_product->name;
		$product->currency = $currency->iso_code;
		$product->price = $price;
		$product->category_id = $ps_product->id_category_default;
		$product->description = $ps_product->description;
		$product->short_description = $ps_product->description_short;
		$product->reference = $ps_product->reference;
		$product->manufacturer = $ps_product->manufacturer_name;
		$product->available = (bool)$ps_product->available_for_order;
		$product->available_msg = $ps_product->available_for_order ? $ps_product->available_now : $ps_product->available_later;
		$product->min_qty = $ps_product->minimal_quantity;
		$product->tags = isset($ps_product->tags[$id_lang]) ? $ps_product->tags[$id_lang] : null;		
		$product->url = $context->link->getProductLink($ps_product);
		
		$product->old_price = $ps_product->show_price ? $ps_product->getPrice(true, null, 2, null, false, false) : null;
		if ($product->price == $product->old_price)
			$product->old_price = null;

		$product->tax_rate = $ps_product->tax_rate;

		// Add images
		$images = $ps_product->getImages($id_lang, $context);
		foreach ($images as $i)
		{
			$product_image = Model\ProductImage::Model();			
			$product_image->id = $i['id_image'];
			$product_image->description = $i['legend'];
			$product_image->is_cover = $i['cover'];
			$product->addImage($product_image);			
		}

		// Add features
		$features = $ps_product->getFrontFeatures($id_lang);
		foreach ($features as $f)
		{
			$feature = Model\ProductFeature::Model();
			$feature->name = $f['name'];
			$feature->value = $f['value'];
			$product->addFeature($feature);
		}

		// Add product attributes
		$attribute_groups = $ps_product->getAttributesGroups($id_lang);		
		
		// Clean up
		$attributes = array();
		foreach ($attribute_groups as $a)
			$attributes[$a['id_attribute']] = $a;

		$groups = array();
		foreach ($attributes as $a)
		{
			$group_id = $a['id_attribute_group'];

			if (!isset($groups[$group_id]))
			{
				$groups[$group_id] = Model\ProductAttributeGroup::Model();
				$groups[$group_id]->id = $group_id;
				$groups[$group_id]->name = $a['group_name'];
				$product->addAttributeGroup($groups[$group_id]);
			}
			
			$value = Model\ProductAttributeValue::Model();
			$value->id = $a['id_attribute'];
			$value->name = $a['attribute_name'];
			
			$groups[$group_id]->addValue($value);
		}
		
		/*
	        // Add custom fields
		if (($cfields = $ps_product->getCustomizationFields($id_lang)))
			foreach ($cfields as $f)
			{
				$type = $f['type'] == 2 ? Model\Product::FIELD_TYPE_IMAGE : Model\Product::FIELD_TYPE_TEXT;
				$product->addField($f['id_customization_field'], $f['name'], $type , null, (bool)$f['required']);
			}
	        */	        
	        return $product;
        }

	public function getProductCombinations($product_id, $lang, Model\Container $combinations)
	{
		$id_lang = $this->getPSLangID($lang);
		$ps_product = new Product($product_id, true, $id_lang);
		$ps_combinations = $ps_product->getAttributeCombinations($id_lang);

		$cbimg = $ps_product->getCombinationImages($id_lang);
		$combs = array();


		// Add combinations to container
              	foreach ($ps_combinations as $c)
              	{
		      	$combination_id = $c['id_product_attribute'];

		      	$combination = isset($combs[$combination_id]) ? $combs[$combination_id] : null;
		      	
		      	if (!$combination)
		      	{
		      		$combination = Model\Combination::Model();
		      		$combination->id = $combination_id;
		      		$combination->price =  $ps_product->show_price ? (float)$ps_product->getPrice(true, $combination_id, 2) : null;
		      		$combination->old_price =  $ps_product->show_price ? (float)$ps_product->getPrice(true, $combination_id, 2, null, false, false) : null;
		      		$combination->cover_id = isset($cbimg[$combination_id][0]) ? $cbimg[$combination_id][0]['id_image'] : 0;
		      		$combination->available = $ps_product->available_for_order && (bool)$c['quantity'];
		      		$combination->available_msg = $combination->available ? $ps_product->available_now : $ps_product->available_later;
		      		
		      		if ($combination->price == $combination->old_price)
		      			$combination->old_price = null;
		      		
			      	$combs[$combination_id] = $combination;			      	
			      	$combinations->addModel($combination);
		      	}
		      	
		      	$group =  Model\ProductAttributeGroup::Model();
		      	$group->id = $c['id_attribute_group'];
			$group->name = $c['group_name'];
			
			$value = Model\ProductAttributeValue::Model();
			$value->id = $c['id_attribute'];
			$value->name = $c['attribute_name'];

			$group->addValue($value);

			$combination->addAttributeGroup($group);
              	}
              	
              	return $combinations;
        }



	public function getProductImage($product_id, $image_id, $lang, $size)
	{
		$id_lang = $this->getPSLangID($lang);

		// Load image object
		$ps_image = new Image($image_id, $id_lang);

		if (!$ps_image->id || $ps_image->id_product != $product_id)
			throw new Exception(Tools::displayError('Image not found'), StatusCode::NOT_FOUND);

		// Find best size for image
		$types = ImageType::getImagesTypes();
		$found = null;		
		foreach ($types as $t)
			if ((int)$t['products'] && (int)$t['width'] >= $size && (!$found || (int)$found['width'] > (int)$t['width']))
				$found = $t;		

		if ($found)
		{
			$type = $found['name'] ? '-'.$found['name'] : '';
			$theme = Shop::isFeatureActive() ? '-'.(int)Context::getContext()->shop->id_theme : '';
			
			if (!file_exists(_PS_PROD_IMG_DIR_.$ps_image->getImgFolder().$image_id.$type.$theme.'.jpg'))
				$theme = '';

			return _PS_PROD_IMG_DIR_.$ps_image->getImgFolder().$image_id.$type.$theme.'.jpg';
		}

		// Set image local path
		return _PS_PROD_IMG_DIR_.$ps_image->getImgPath().'.jpg';
	}

	public function getCart($cart_id, $lang, Model\Cart $cart)
	{
		$id_lang = $this->getPSLangID($lang);

		static $_cart = null;
		if (!$_cart)
		{
			$context = Context::getContext();

			// Get cart details
			$ps_cart = $this->getPSCart($cart_id);
			$currency = new Currency($ps_cart->id_currency);
			$summary = $ps_cart->getSummaryDetails();

			$cart->id = $ps_cart->id;
			$cart->total_products = (float)$summary['total_products_wt'];
			$cart->total_shipping = (float)$summary['total_shipping'];
			$cart->total_tax = (float)$summary['total_tax'];
			$cart->total = (float)$summary['total_price'];
			$cart->shipping_tax = (float)$summary['total_shipping'] - (float)$summary['total_shipping_tax_exc'];
			$cart->products_tax = (float)$summary['total_products_wt'] - (float)$summary['total_products'];
			$cart->currency = $currency->iso_code;
			$cart->url = $context->link->getPageLink('cart', null, $id_lang);
			$cart->token = $ps_cart->secure_key;

			// Get products
			foreach ($summary['products'] as $p)
			{
				$product = Model\CartProduct::Model();

				$product->id = $p['id_product'] . '-' . $p['id_product_attribute'];
				$product->product_id = (int)$p['id_product'];
				$product->combination_id = (int)$p['id_product_attribute'];
				$product->name = $p['name'];
				$product->quantity = $p['quantity'];
				$product->price = (float)$p['total_wt'];
				$product->details = isset($p['attributes']) ? $p['attributes'] : null;

				if ($p['id_image'])
				{
					$img_ids = explode('-', $p['id_image']);
					$product->cover_id = (int)$img_ids[1];
				}

				$cart->addProduct($product);
			}

			// Set selected delivery address
			if ($ps_cart->id_address_delivery)
			{
				$ps_address = new Address($ps_cart->id_address_delivery);
				$ps_country = new Country($ps_address->id_country);
				$ps_state = new State($ps_address->id_state);

				$address = Model\Address::Model();

				$address->id =		$ps_address->id;
				$address->firstname = 	$ps_address->firstname;
				$address->lastname = 	$ps_address->lastname;
				$address->company =	$ps_address->company;
				$address->line1 = 	$ps_address->address1;
				$address->line2 = 	$ps_address->address2;
				$address->zip =		$ps_address->postcode;
				$address->city = 	$ps_address->city;
				$address->phone1 =	$ps_address->phone;
				$address->phone2 = 	$ps_address->phone_mobile;
				$address->vat  =	$ps_address->vat_number;
				$address->dni = 	$ps_address->dni;
				$address->country =	$ps_country->iso_code;
				$address->state = 	$ps_state->iso_code;

				$cart->setShippingAddress($address);
			}

			if ($ps_cart->id_carrier)
			{
				$ps_carrier = new Carrier($ps_cart->id_carrier, $id_lang);
				$carrier = Model\Carrier::Model();
				$carrier->id = $ps_carrier->id;
				$carrier->name = $ps_carrier->name;
				$carrier->details = $ps_carrier->delay;
				$carrier->price = (float)$summary['total_shipping'];
				$cart->carrier = $carrier;
			}

			$_cart = $cart;
		}
	        return $_cart;
        }

	public function clearCart($cart_id)
	{
		$cart = $this->getCart($cart_id, null, Model\Cart::Model());
	
		foreach ($cart->products as $cp)
			if (!$this->cartRemoveProduct($cart_id, $cp->id))
				return false;
		return true;
	}

	public function cartRemoveProduct($cart_id, $cart_product_id)
	{
		list($product_id, $combination_id) = explode('-', $cart_product_id.'--', 3);
		$this->getPSCartController()->deleteProduct($product_id, $combination_id);
		return true;
	}

	public function cartAddProduct($cart_id, Model\CartProduct $cartProduct)
	{
		$this->getPSCartController()->updateProduct($cartProduct->product_id, $cartProduct->combination_id, $cartProduct->quantity);

		$cart = $this->getCart($cart_id, null, Model\Cart::Model());
		foreach ($cart->products as $p)
			if ($p->product_id == $cartProduct->product_id && $p->combination_id == $cartProduct->combination_id)
				return $p;

		throw new Exception(Tools::displayError('Unable to add product'), StatusCode::INTERNAL_SERVER_ERROR);
	}

	public function cartUpdateProduct($cart_id, $cart_product_id, Model\CartProduct $cartProduct)
	{
		$cart = $this->getCart($cart_id, null, Model\Cart::Model());

		foreach ($cart->products as $cp)
			if ($cp->id == $cart_product_id)
			{
				$this->getPSCartController()->updateProduct($cp->product_id, $cp->combination_id, (int)$cartProduct->quantity - (int)$cp->quantity);
				return true;
			}

		throw new Exception(Tools::displayError('Product not found'), StatusCode::NOT_FOUND);
	}



	public function cartSetShippingAddress($cart_id, Model\Address $address)
	{
		$id_lang = Configuration::get('PS_LANG_DEFAULT');

		// Get customer
		$ps_customer = $this->getPSCustomer();

		// Validate all address fields
		if (!Validate::isLanguageIsoCode($address->country))
			throw new Exception(Tools::displayError('Invalid country code'), StatusCode::BAD_REQUEST);
		if (!Validate::isName($address->firstname))
			throw new Exception(Tools::displayError('Invalid first name'), StatusCode::BAD_REQUEST);
		if (!Validate::isName($address->lastname))
			throw new Exception(Tools::displayError('Invalid last name'), StatusCode::BAD_REQUEST);
		if (!Validate::isCityName($address->city))
			throw new Exception(Tools::displayError('Invalid first name'), StatusCode::BAD_REQUEST);
		if (!Validate::isAddress($address->line1))
			throw new Exception(Tools::displayError('Invalid first address line'), StatusCode::BAD_REQUEST);
		if ($address->line1 && !Validate::isAddress($address->line2))
			throw new Exception(Tools::displayError('Invalid second address line'), StatusCode::BAD_REQUEST);
		if ($address->company && !Validate::isGenericName($address->company))
			throw new Exception(Tools::displayError('Invalid company name'), StatusCode::BAD_REQUEST);
		if ($address->vat && !Validate::isGenericName($address->vat))
			throw new Exception(Tools::displayError('Invalid vat number'), StatusCode::BAD_REQUEST);
		if ($address->zip && !Validate::isPostCode($address->zip))
			throw new Exception(Tools::displayError('Invalid zip code'), StatusCode::BAD_REQUEST);
		if ($address->phone1 && !Validate::isPhoneNumber($address->phone1))
			throw new Exception(Tools::displayError('Invalid primary phone number'), StatusCode::BAD_REQUEST);
		if ($address->phone2 && !Validate::isPhoneNumber($address->phone2))
			throw new Exception(Tools::displayError('Invalid secondary phone number'), StatusCode::BAD_REQUEST);
		if ($address->dni && !Validate::isDniLite($address->dni))
			throw new Exception(Tools::displayError('Invalid DNI number'), StatusCode::BAD_REQUEST);


		// Find country id
		if (!($id_country = Country::getByIso($address->country)))
			throw new Exception(Tools::displayError('Country is not available'), StatusCode::BAD_REQUEST);

		// Generate address alias name
		$alias = 'Shopacitor: '.Tools::substr(md5(preg_replace('/[^a-z0-9]/', '', Tools::strtolower(implode('', array_filter(get_object_vars($address)))))), 3, 16);

		// Find address by alias
		$id_address = (int)Db::getInstance()->getValue(sprintf("SELECT `id_address` FROM `%saddress` WHERE `id_customer`=%d AND `alias`='%s'", _DB_PREFIX_, $ps_customer->id, $alias));

		// Creata address object
		$ps_address = new Address($id_address);

		// Set address fields
		if (!$ps_address->id)
		{
			$ps_address->id_customer = $ps_customer->id;
			$ps_address->id_country = $id_country;
			$ps_address->id_state = State::getIdByIso($address->state, $id_country);
			$ps_address->country = Country::getNameById($id_lang, $id_country);
			$ps_address->alias = $alias;
			$ps_address->company = $address->company;
			$ps_address->lastname = $address->lastname;
			$ps_address->firstname = $address->firstname;
			$ps_address->address1 = $address->line1;
			$ps_address->address2 = $address->line2;
			$ps_address->postcode = $address->zip;
			$ps_address->city = $address->city;
			$ps_address->phone = $address->phone1;
			$ps_address->phone_mobile = $address->phone2;
			$ps_address->vat_number = $address->vat;
			$ps_address->dni = $address->dni;

			try {
				$ps_address->add();
			} catch (Exception $e) {
				throw new Exception($e->getMessage(), StatusCode::BAD_REQUEST);
			}
		}

		// Get current cart
		$ps_cart = $this->getPSCart($cart_id);

		// Set cart address and customer
		$ps_cart->id_address_delivery =	(int)$ps_address->id;
		$ps_cart->id_address_invoice =	(int)$ps_address->id;
		$ps_cart->id_customer =		(int)$ps_customer->id;
		$ps_cart->secure_key = 		$ps_customer->secure_key;
		try {
			$ps_cart->save();
		} catch (Exception $e) {
			throw new Exception($e->getMessage(), StatusCode::BAD_REQUEST);
		}

		return true;
	}


	public function cartGetAvailableCarriers($cart_id, $lang, Model\Container $carriers)
	{
		$ps_cart = $this->getPSCart($cart_id);
		$id_lang = $this->getPSLangID($lang);

		if (!(int)$ps_cart->id_address_delivery)
			throw new Exception(Tools::displayError('Please set shipping address first'), StatusCode::BAD_REQUEST);

		$ps_address = new Address($ps_cart->id_address_delivery);
		$ps_country = new Country($ps_address->id_country);

		$delivery_option_list = $ps_cart->getDeliveryOptionList($ps_country);
		$ps_carriers = is_array($delivery_option_list) && count($delivery_option_list) ? reset($delivery_option_list) : array();

		foreach ($ps_carriers as $key => $option)
		{
			if ($option['unique_carrier'])
			{
				$first= reset($option['carrier_list']);
				$ps_carrier = $first['instance'];

				$carrier = Model\Carrier::Model();

				$carrier->id = $ps_carrier->id;
				$carrier->name = $ps_carrier->name;
				$carrier->details = isset($ps_carrier->delay[$id_lang]) ? $ps_carrier->delay[$id_lang] : reset($ps_carrier->delay);
				$carrier->price = $option['total_price_with_tax'];

				$carriers->addModel($carrier);
			}
		}

		return $carriers;
	}

	public function cartSelectCarrier($cart_id, $carrier_id)
	{
		$ps_cart = $this->getPSCart($cart_id);
		if (!(int)$ps_cart->id_address_delivery)
			throw new Exception(Tools::displayError('Select delivery address first'), StatusCode::BAD_REQUEST);

		$id_lang = $this->getPSLangID();
		$ps_carrier = new Carrier($carrier_id, $id_lang);

		// Validate id
		if (!$ps_carrier || $ps_carrier->id == 0 || $ps_carrier->deleted || !$ps_carrier->active)
			throw new Exception(Tools::displayError('Carrier not found'), StatusCode::NOT_FOUND);

		// Select carrier
		$delivery_option = array($ps_cart->id_address_delivery => $ps_carrier->id.',');
		$ps_cart->setDeliveryOption($delivery_option);

		// Validate selected carrier
		if ($ps_cart->id_carrier != $ps_carrier->id)
			throw new Exception(Tools::displayError('Carrier is not available'), StatusCode::FORBIDDEN);


		Hook::exec('actionCarrierProcess', array('cart' => $ps_cart));

		$r = $ps_cart->update();

		// Carrier has changed, so we check if the cart rules still apply
		$context = Context::getContext();
		CartRule::autoRemoveFromCart($context);
		CartRule::autoAddToCart($context);

		return $r;
	}

	public function cartGetPaypalPaymentMethod($cart_id, Model\PayPalPaymentMethod $paypal)
	{
		$paypal->merchant_email = Configuration::get('PAYPAL_BUSINESS_ACCOUNT');
		return $paypal;
	}

	public function registerCustomer(Model\Customer $customer)
	{
		// Validate customer
		if (!$customer->firstname)
			throw new Exception(Tools::displayError('Missing first name'), StatusCode::BAD_REQUEST);
		if (!$customer->lastname)
			throw new Exception(Tools::displayError('Missing last name'), StatusCode::BAD_REQUEST);
		if (!$customer->password)
			throw new Exception(Tools::displayError('Password is missing'), StatusCode::BAD_REQUEST);
		if (!Validate::isEmail($customer->email))
			throw new Exception(Tools::displayError('Invalid email address'), StatusCode::BAD_REQUEST);
		if (Customer::customerExists($customer->email))
			throw new Exception(Tools::displayError('An account using this email address has already been registered.'), StatusCode::CONFLICT);

		$ps_customer = new Customer();

		$ps_customer->firstname = 	$customer->firstname;
		$ps_customer->lastname = 	$customer->lastname;
		$ps_customer->email = 		$customer->email;
		$ps_customer->company = 	$customer->company;
		$ps_customer->passwd = 		Tools::encrypt($customer->password);

		if (!$ps_customer->add())
			throw new Exception(Tools::displayError('An error occurred while creating your account.'), StatusCode::INTERNAL_SERVER_ERROR);

		$customer->id = $ps_customer->id;
		$customer->password = null;
		return $customer;
	}

	public function getCustomer($customer_id, Model\Customer $customer)
	{
		$ps_customer = $this->getPSCustomer();

		if ($customer_id && $customer_id != $ps_customer->id)
			throw new Exception(Tools::displayError('Invalid user ID'), StatusCode::BAD_REQUEST);

		$customer->id = $ps_customer->id;
		$customer->firstname = $ps_customer->firstname;
		$customer->lastname = $ps_customer->lastname;
		$customer->email = $ps_customer->email;
		$customer->company = $ps_customer->company;

		return $customer;
	}

	public function createOrder(Model\Order $order)
	{
		// Validate customer
		$ps_customer = $this->getPSCustomer();

		// Get cart for order
		$ps_cart = $this->getPSCart($order->cart_id);

		$id_currecy = Currency::getIdByIsoCode($order->currency);
		$currency = new Currency($id_currecy);
		if (!Validate::isLoadedObject($currency) || !$currency->active || $currency->deleted)
			throw new Exception(Tools::displayError('Currency is not accepted'), StatusCode::NOT_FOUND);

		// Validate payment method
		$payment_method = $this->cartGetPaymentMethod($order->cart_id, $order->payment_method_id);

		// Update cart currency
		if ($currency->id != $ps_cart->id_currency)
		{
			$ps_cart->id_currency = $currency->id;
			$ps_cart->save();
		}

		// Set order details
		$order->id = (int)$ps_cart->id;
		$order->cart_id = (int)$ps_cart->id;
		$order->carrier_id = (int)$ps_cart->id_carrier;
		$order->payment_status = Model\Order::PAYMENT_STATUS_PENDING;
		$order->total_amount = round(Tools::convertPrice($ps_cart->getOrderTotal(true), $ps_cart->id_currency), 2);
		$order->currency = $currency->iso_code;
		$order->custom_data = null;

		return $order;
	}

	public function updateOrder($order_id, Model\Order $order)
	{
		$ps_cart = new Cart($order_id);
		if (!Validate::isLoadedObject($ps_cart))
			throw new Exception(Tools::displayError('Order not found'), StatusCode::NOT_FOUND);

		// Get cart currency
		$id_currecy = Currency::getIdByIsoCode($order->currency);
		$currency = new Currency($id_currecy);
		if (!Validate::isLoadedObject($currency) || !$currency->active || $currency->deleted)
			throw new Exception(Tools::displayError('Currency is not accepted'), StatusCode::NOT_FOUND);

		// Get payment method
		$payment_method = $this->cartGetPaymentMethod($order->cart_id, $order->payment_method_id);

		// Get order state id
		$id_order_state = Configuration::get('PS_OS_ERROR');
		switch($order->payment_status)
		{
			case Model\Order::PAYMENT_STATUS_PENDING:
				$id_order_state  = Configuration::get('PS_OS_PAYPAL');
				break;
			case Model\Order::PAYMENT_STATUS_COMPLETED:
				$id_order_state  = Configuration::get('PS_OS_PAYMENT');
				break;
			case Model\Order::PAYMENT_STATUS_CANCELED:
				$id_order_state  = Configuration::get('PS_OS_CANCELED');
				break;
			case Model\Order::PAYMENT_STATUS_ERROR:
				$id_order_state  = Configuration::get('PS_OS_ERROR');
				break;
		}

		if ($ps_cart->orderExists())
		{
			$id_order = Order::getOrderByCartId($order_id);
			$ps_order = new Order($id_order);
			if ($ps_order->getCurrentState() != $id_order_state)
				$ps_order->setCurrentState($id_order_state);
		}
		else
		{
			//$ps_customer = new Customer($ps_cart->id_customer);
			$module = new Shopacitor();
			try {
				$module->validateOrder(
				    $ps_cart->id,
				    $id_order_state,
				    $order->total_amount,
				    $payment_method->name,
				    null,
				    array('transaction_id'=> $order->payment_transaction_id),
				    $id_currecy,
				    false,
				    $ps_cart->secure_key
				);
			} catch (PrestaShopException $e) {
				throw new Exception($e->getMessage(), StatusCode::BAD_REQUEST);
			}
		}
		return true;
	}

	public function getPages($lang, Model\Container $pages)
	{
		$id_lang = $this->getPSLangID($lang);
		$list = CMS::listCms($id_lang);

		$id_terms = Configuration::get('PS_CONDITIONS_CMS_ID');

		foreach ($list as $p)
		{
			$page = Model\Page::Model();
			$page->id = $p['id_cms'];
			$page->title = $p['meta_title'];
			$page->type = ($page->id == $id_terms) ? Model\Page::TYPE_TERMS : Model\Page::TYPE_OTHER;
			$pages->addModel($page);
		}

		return $pages;
	}

	public function getPage($page_id, $lang, Model\Page $page)
	{
		$id_lang = $this->getPSLangID($lang);
		$cms = new CMS($page_id, $id_lang);

		if (!Validate::isLoadedObject($cms) || !$cms->active)
			throw new Exception(Tools::displayError('Page not found'), StatusCode::NOT_FOUND);

		$page->id = $page_id;
		$page->title = $cms->meta_title;
		$page->text = $cms->content;
		$page->type = ($page->id == Configuration::get('PS_CONDITIONS_CMS_ID')) ? Model\Page::TYPE_TERMS : Model\Page::TYPE_OTHER;

		return $page;
	}

}
