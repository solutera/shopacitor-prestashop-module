{**
 *  Shopacitor Module for Prestashop
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 *}

{extends file="helpers/form/form.tpl"}

{block name="input"}
	{if $input.type == 'image_preview'}
    		<img src="{$fields_value[$input.name]|escape:'htmlall'}" width="128" height="128"/>
    	{else}
    		{$smarty.block.parent}
    	{/if}
{/block}

