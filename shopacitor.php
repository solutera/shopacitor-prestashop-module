<?php
/**
 *  Shopacitor Module for Prestashop
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

if (!defined('_PS_VERSION_'))
	exit;

require_once('classes/PSMerchantApi.php');

class Shopacitor extends PaymentModule
{
	private $defaults = array
	(
		'SHOPACITOR_SHOP_ID' => '',
		'SHOPACITOR_SHOP_API_KEY' => '',
		'SHOPACITOR_SHOP_MODE' => \Shopacitor\Model\Shop::MODE_NONE,
		'SHOPACITOR_SHOP_EMAIL' => ''
	);

	public function __construct()
	{
		$this->name = 'shopacitor';
		$this->tab = 'market_place';
		$this->version = '1.0';
		$this->author = 'Solutera';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.5.0', 'max' => '1.6.0.14');
		$this->module_key = '';
		
		parent::__construct();
		
		$this->displayName = 'Shopacitor';
		$this->description = $this->l('Sell your products on Shopacitor app.');        
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

		if (!function_exists('curl_init'))
			$this->warning = $this->l('Missing PHP cURL extension');
	}

	public function install()
	{
		if
		(
			!parent::install() ||
			// TODO hook Header
			!$this->registerHook('actionProductAdd') ||
			!$this->registerHook('actionProductSave') ||
			!$this->registerHook('actionProductUpdate') ||
			!$this->registerHook('actionProductDelete')
		)
		return false;
	
		$this->defaults['SHOPACITOR_SHOP_EMAIL'] = Configuration::get('PS_SHOP_EMAIL');
	 
		// Init configurations 
		foreach ($this->defaults as $name => $value)
		if (!Configuration::updateValue($name, $value))
			return false;
	
		return true;
	}
	
	public function uninstall()
	{
		if (!$this->disableShop())
			return false;
	
		// Remove configurations
		foreach ($this->defaults as $name => $value)
			if (!Configuration::deleteByName($name))
				return false;
	
		return parent::uninstall();
	}
	
	public function disable($forceAll = false)
	{
		return $this->disableShop() && parent::disable($forceAll);
	}
	
	private function disableShop()
	{
		Configuration::updateValue('SHOPACITOR_SHOP_MODE', \Shopacitor\Model\Shop::MODE_NONE);

		$shop = $this->getShopModel();
		$error = null;
		if ($shop->id && !$this->api()->updateShop($shop))
			return false;		
		
		return true;
	}

/*	
	public function hookHeader()
	{
		// TODO check if mobile & send headers to suggest app
		// Smart banner
	}
*/

	public function hookActionProductAdd($params)
	{
		if (!$this->api()->shop_id)
			return;

		$ps_product = $params['product'];

		if ($ps_product->active)
		{
			$product = $this->api()->getProduct($ps_product->id, null, \Shopacitor\Model\Product::Model());
			try {
				$this->api()->addProduct($product);
			} catch (Exception $e) {
				$this->context->controller->errors[] = $this->displayName . ': ' . $e->getMessage();
			}
		}
	}

	public function hookActionProductSave($params)
	{
		$product = new Product($params['id_product']);
		$this->hookActionProductUpdate(array('product'=>$product));
	}

	public function hookActionProductUpdate($params)
	{
		if (!$this->api()->shop_id)
			return;

		$ps_product = $params['product'];

		try {
			if ($ps_product->active)
			{
				$product = $this->api()->getProduct($ps_product->id, null, \Shopacitor\Model\Product::Model());
				$this->api()->updateProduct($product);
			}
			else
				$this->api()->deleteProduct($ps_product->id);
		}
		catch(Exception $e)
		{
			$this->context->controller->errors[] = $this->displayName.': '.$e->getMessage();
		}
	}

	public function hookActionProductDelete($params)
	{
		$product = $params['product'];
		$api = $this->api();
		if ($api->shop_id)
		{
			try {
				$api->deleteProduct($product->id);
			} catch (Exception $e) {
				$this->context->controller->errors[] = $this->displayName . ': ' . $e->getMessage();
			}
		}
	}

	public function getContent()
	{
		return $this->postProcess().$this->displayForm();
	}
	
	private function logoPath()
	{
		return $this->name.'/resources/logo4shopacitor.jpg';
	}
	
	private function logoURL()
	{
		if (file_exists(($file = _PS_MODULE_DIR_.$this->logoPath())))
			return $this->context->link->protocol_content.Tools::getMediaServer($this->name)._MODULE_DIR_.$this->logoPath().'?'.filemtime($file);

		return null;
	}
	
	public function postProcess()
	{
		$output = '';
		$errors = array();

		if (Tools::isSubmit('submit'.$this->name))
		{
			// Check and validate Shop ID
			if (($shop_id = Tools::getValue('SHOPACITOR_SHOP_ID')) && !Validate::isUnsignedId($shop_id))
				array_push($errors, $this->l('Invalid Shop ID value'));

			// Check and validate Api Key
			if (($api_key = Tools::getValue('SHOPACITOR_SHOP_API_KEY')) && !Validate::isSha1($api_key))
				array_push($errors, $this->l('Invalid API Key value'));

			// Check and validate Contact Email address
			if (!($email = Tools::getValue('SHOPACITOR_SHOP_EMAIL')) || !Validate::isEmail($email))
				array_push($errors, $this->l('Invalid Email address'));

			// Check and validate Paypal email
			if (!($paypal_email = Tools::getValue('PAYPAL_BUSINESS_ACCOUNT')) || !Validate::isEmail($paypal_email))
				array_push($errors, $this->l('Invalid PayPal Email address'));


			// Validate, upload and resize logo image
			$tmpName = null;
			if ($_FILES['SHOPACITOR_SHOP_LOGO']['name'])
			{
				if (($error = ImageManager::validateUpload($_FILES['SHOPACITOR_SHOP_LOGO'])))
					array_push($errors, $error);
				elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')))
					array_push($errors, $this->l('Unable to create temp file.'));
				elseif (!move_uploaded_file($_FILES['SHOPACITOR_SHOP_LOGO']['tmp_name'], $tmpName))
					array_push($errors, $this->l('Unable to upload file.'));
				elseif (!ImageManager::resize($tmpName, _PS_MODULE_DIR_.$this->logoPath(), 512, 512))
					array_push($errors, $this->l('Unable to resize logo file.'));
					
				if ($tmpName)
					unlink($tmpName);
			}

			if (!count($errors))
			{
				// Update local settings
				foreach ($this->defaults as $name => $default_value)
					Configuration::updateValue($name, Tools::getValue($name, $default_value));
		
				$shop = $this->getShopModel();												
				$api = $this->api();
				$error = null;
				
				try
				{
					// Update shop
					if ($shop->id && $api->updateShop($shop))
						$output .= $this->displayConfirmation($this->l('Settings updated'));

					// Register shop
					elseif (!$shop->id && ($shop = $api->addShop($shop)))
					{
						$api->shop_id = $shop->id;
						$api->api_key = $shop->api_key;
						unset($_POST['SHOPACITOR_SHOP_ID'], $_POST['SHOPACITOR_SHOP_API_KEY']);

						Configuration::updateValue('SHOPACITOR_SHOP_ID', $shop->id);
						Configuration::updateValue('SHOPACITOR_SHOP_API_KEY', $shop->api_key);

						$output .= $this->displayConfirmation($this->l('Shop successfully registered'));
					}
				}
				catch (Exception $e)
				{
					array_push($errors, $this->l('Error from server'). ': '. $e->getMessage());
				}
			}
			
			if (count($errors))
				$output .= $this->displayError(implode('<br />', $errors));
		}
		
		return $output;
	}
	
    
	public function displayForm()
	{
		$output = '';
	
		// Get default Language
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		
		// Get current shop status on SHOPACITOR
		$status_names = array
		(
			\Shopacitor\Model\Shop::STATUS_CONFIRMED =>	$this->l('Confirmed'),
			\Shopacitor\Model\Shop::STATUS_PROCESSING =>	$this->l('Processing'),
			\Shopacitor\Model\Shop::STATUS_WAITING => 	$this->l('Waiting'),
			\Shopacitor\Model\Shop::STATUS_REJECTED => 	$this->l('Rejected'),
			\Shopacitor\Model\Shop::STATUS_UNKNOWN => 	$this->l('Unknown')
		);
		
		
		// Check status if we have shop ID
		$shopModel = null;
		if ((int)Configuration::get('SHOPACITOR_SHOP_ID'))
		{
			try{
				$shopModel =  $this->api()->getShop();
			}
			catch (Exception $e){
				$output .= $this->displayError($this->l('Error from server'). ': '. $e->getMessage());
			}
		}

		// Init Fields form array
		$fields_form = array();

		$fields_form[0]['form'] = array
		(
			'legend' => array('title' => $this->l('Information')),
			'input' => array
			(
				array
				(
					'type' => 'text',
					'label' => $this->l('Status'),
					'desc' => $shopModel ? $shopModel->status_msg : '',
					'name' => 'SHOPACITOR_SHOP_STATUS',
					'size' => 40,
					'required' => false,
					'readonly' => true
				),
				array
				(
					'type' => 'text',
					'label' => $this->l('Name'),
					'name' => 'SHOPACITOR_SHOP_NAME',
					'size' => 40,
					'required' => false,
					'readonly' => true
				),
				array
				(
					'type' => 'text',
					'label' => $this->l('Domain'),
					'name' => 'SHOPACITOR_SHOP_DOMAIN',
					'size' => 40,
					'required' => false,
					'readonly' => true
				),
				array
				(
					'type' => 'image_preview',
					'label' => $this->l('Logo'),
					'name' => 'SHOPACITOR_SHOP_LOGO',
					'required' => false
				)
			)
		);
		
		$fields_form[1]['form'] = array
		(
			'legend' => array('title' => $this->l('Settings')),
			'input' => array
			(
				array
				(
					'type' => 'radio',
					'label' => $this->l('Mode'),
					'name' => 'SHOPACITOR_SHOP_MODE',
					'br' => true,
					'class' => 't',
					'values' => array
					(
						array('id'=>'shopacitor_mode_public', 	'value'=>\Shopacitor\Model\Shop::MODE_ACTIVE,	'label'=>$this->l('Active')),
						array('id'=>'shopacitor_mode_test', 	'value'=>\Shopacitor\Model\Shop::MODE_TEST,  	'label'=>$this->l('Test')),
						array('id'=>'shopacitor_mode_none', 	'value'=>\Shopacitor\Model\Shop::MODE_NONE,  	'label'=>$this->l('None'))
					),
					'required' => false
				),
				array
				(
					'type' => 'text',
					'label' => $this->l('Shop ID'),
					'name' => 'SHOPACITOR_SHOP_ID',
					'size' => 10,
					'required' => false
				),
				array
				(
					'type' => 'text',
					'label' => $this->l('API Key'),
					'name' => 'SHOPACITOR_SHOP_API_KEY',
					'size' => 40,
					'required' => false
				),
				array
				(
					'type' => 'text',
					'label' => $this->l('Contact email'),
					'name' => 'SHOPACITOR_SHOP_EMAIL',
					'size' => 40,
					'required' => true
				),
				array
				(
					'type' => 'file',
					'label' => $this->l('Logo'),
					'desc' => $this->l('Your shop logo on Shopacitor App'),
					'name' => 'SHOPACITOR_SHOP_LOGO',
					'size' => 40,
					'required' => true
				),
			    	array
			    	(
					'type' => 'text',
					'label' => $this->l('PayPal Business account e-mail'),
					'name' => 'PAYPAL_BUSINESS_ACCOUNT',
					'size' => 40,
					'required' => true
			    	)
			),
			'submit' => array
			(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);

		
		$helper = new HelperForm();
		
		// Module, token and currentIndex
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		
		// Language
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		
		// Title and toolbar
		$helper->title = $this->displayName;
		$helper->show_toolbar = true;        // false -> remove toolbar
		$helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
		$helper->submit_action = 'submit'.$this->name;
		
		$helper->toolbar_btn['save'] = array
		(
			'desc' => $shopModel ? $this->l('Save') : $this->l('Register'),
			'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
			'&token='.Tools::getAdminTokenLite('AdminModules'),
		);
		
		$helper->toolbar_btn['back'] = array
		(
			'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
			'desc' => $this->l('Back to list')
		);

			
		// Load current values
		$helper->fields_value = array
		(
			'SHOPACITOR_SHOP_ID' => $this->formValue('SHOPACITOR_SHOP_ID'),
			'SHOPACITOR_SHOP_API_KEY' => $this->formValue('SHOPACITOR_SHOP_API_KEY'),
			'SHOPACITOR_SHOP_MODE' => $shopModel ? $shopModel->mode : $this->formValue('SHOPACITOR_SHOP_MODE'),
		    	'SHOPACITOR_SHOP_EMAIL' => $this->formValue('SHOPACITOR_SHOP_EMAIL'),
		    	'PAYPAL_BUSINESS_ACCOUNT' => $this->formValue('PAYPAL_BUSINESS_ACCOUNT'),

			'SHOPACITOR_SHOP_NAME' => Configuration::get('PS_SHOP_NAME'),
			'SHOPACITOR_SHOP_DOMAIN' => $_SERVER['SERVER_NAME'],
			'SHOPACITOR_SHOP_STATUS' => $shopModel ? $status_names[$shopModel->status] : $this->l('Unregistered'),
			'SHOPACITOR_SHOP_LOGO' => $this->logoURL()
		);
		
		
		return $output.$helper->generateForm($fields_form);
	}
    
	private function formValue($name)
	{
		return Tools::safeOutput(Tools::getValue($name, Configuration::get($name)));
	}

	/**
	 * @return PSMerchantApi
	 */
	private function api()
	{
		static $api = null;
	
		if (!$api)
			$api = PSMerchantApi::ApiWithCredentials(Configuration::get('SHOPACITOR_SHOP_ID'), Configuration::get('SHOPACITOR_SHOP_API_KEY'), 'Prestashop '._PS_VERSION_);
			
		return $api;
	}

	private function getShopModel()
	{
		$shop = \Shopacitor\Model\Shop::Model();

		$shop->id = 		Configuration::get('SHOPACITOR_SHOP_ID');
		$shop->email = 		Configuration::get('SHOPACITOR_SHOP_EMAIL');
		$shop->mode = 		Configuration::get('SHOPACITOR_SHOP_MODE');
		$shop->name = 		Configuration::get('PS_SHOP_NAME');
		$shop->domain = 	$_SERVER['SERVER_NAME'];
		$shop->api_url = 	str_replace('%7Bresource%7D', '{resource}', $this->context->link->getModuleLink($this->name, 'Api', array('resource'=>'{resource}')));				
		
		return $shop;		
	}
}
