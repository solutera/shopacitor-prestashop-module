<?php
/**
 *  Shopacitor Merchant API
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

namespace Shopacitor\HTTP
{
	use \Shopacitor\URL,
	    \Shopacitor\Object;

	class Method
	{
		const OPTIONS = 'OPTIONS';
		const GET = 	'GET';
		const HEAD =	'HEAD';
		const POST =	'POST';
		const PUT =	'PUT';
		const DELETE =	'DELETE';
		const TRACE = 	'TRACE';
		const CONNECT = 'CONNECT';
	}

	class ContentType
	{
		const TEXT_PLAIN = 		'text/plain';
		const APPLICATION_JSON =	'application/json';
		const APPLICATION_FORM =	'application/x-www-form-urlencoded';
		const APPLICATION_FILE =	'application/octet-stream';
		const IMAGE =			'image/*';
		const IMAGE_GIF =		'image/gif';
		const IMAGE_PNG =		'image/png';
		const IMAGE_JPG =		'image/jpg';
		const IMAGE_JPEG =		'image/jpeg';
	}

	class HeaderField
	{
		const ACCEPT = 			'Accept';
		const ALLOW = 			'Allow';
		const ACCEPT_LANGUAGE =		'Accept-Language';
		const AUTHORIZATION =		'Authorization';
		const CONTENT_TYPE = 		'Content-Type';
		const CONTENT_LANGUAGE =	'Content-Language';
		const CONTENT_RANGE = 		'Content-Range';
		const CONTENT_LENGTH =		'Content-Length';
		const CONTENT_LOCATION =	'Content-Location';
		const RANGE =			'Range';
		const IF_MODIFIED_SINCE=	'If-Modified-Since';
		const LOCATION = 		'Location';
		const WWW_AUTHENTICATE = 	'WWW-Authenticate';
		const LAST_MODIFIED = 		'Last-Modified';
		const PRAGMA = 			'Pragma';
		const EXPECT = 			'Expect';
	}

	class StatusCode
	{
		const CONTINUE_ = 		100;
		const OK = 			200;
		const CREATED = 		201;
		const NO_CONTENT = 		204;
		const BAD_REQUEST =		400;
		const UNAUTHORISED =		401;
		const FORBIDDEN = 		403;
		const NOT_FOUND = 		404;
		const METHOD_NOT_ALLOWED = 	405;
		const REQUEST_TIMEOUT = 	408;
		const CONFLICT = 		409;
		const EXPECTATION_FAILED =	417;
		const INTERNAL_SERVER_ERROR = 	500;
		const NOT_IMPLEMENTED = 	501;
		const SERVICE_UNAVAILABLE = 	503;

		private static $status_code_names = array
		(
		    0 => 'Unknown',
		    100 => 'Continue',
		    101 => 'Switching Protocols',
		    200 => 'OK',
		    201 => 'Created',
		    202 => 'Accepted',
		    203 => 'Non-Authoritative Information',
		    204 => 'No Content',
		    205 => 'Reset Content',
		    206 => 'Partial Content',
		    300 => 'Multiple Choices',
		    301 => 'Moved Permanently',
		    302 => 'Found',
		    303 => 'See Other',
		    304 => 'Not Modified',
		    305 => 'Use Proxy',
		    306 => '(Unused)',
		    307 => 'Temporary Redirect',
		    400 => 'Bad Request',
		    401 => 'Unauthorised',
		    402 => 'Payment Required',
		    403 => 'Forbidden',
		    404 => 'Not Found',
		    405 => 'Method Not Allowed',
		    406 => 'Not Acceptable',
		    407 => 'Proxy Authentication Required',
		    408 => 'Request Timeout',
		    409 => 'Conflict',
		    410 => 'Gone',
		    411 => 'Length Required',
		    412 => 'Precondition Failed',
		    413 => 'Request Entity Too Large',
		    414 => 'Request-URI Too Long',
		    415 => 'Unsupported Media Type',
		    416 => 'Requested Range Not Satisfiable',
		    417 => 'Expectation Failed',
		    500 => 'Internal Server Error',
		    501 => 'Not Implemented',
		    502 => 'Bad Gateway',
		    503 => 'Service Unavailable',
		    504 => 'Gateway Timeout',
		    505 => 'HTTP Version Not Supported'
		);

		/**
		 * @param int $code
		 * @return string|null
		 */
		static public function Description($code)
		{
			return array_key_exists($code, self::$status_code_names) ? self::$status_code_names[$code] : null;
		}
	}

	abstract class Connection extends Object
	{
		protected $errors = array();

		public function __invoke()
		{
			$this->invoke();
		}

		public function invoke()
		{
			$request = Request::Alloc()->initWithConnection($this);
			$response = $this->handleRequest($request);
			if ($response)
				$response->send();
		}

		public function sendRequest(Request $request)
		{
			return $request->send();
		}

		/**
		 * Handle incoming request
		 * @param Request $request
		 * @return Response
		 */
		abstract protected function handleRequest(Request $request);
	}

	class Exception extends \Exception
	{
		public function __construct($code)
		{
			parent::__construct(StatusCode::Description($code), $code);
		}
	}

	class Message extends Object
	{
		/** @var \Shopacitor\URL  */
		protected $url = null;

		/** @var array */
		protected $headers = array();

		protected $cookie = null;
		protected $data = null;
		protected $method = Method::GET;

		/**
		 * @param URL|string $url
		 */
		public function setURL($url)
		{
			$this->url = is_object($url) ? $url : URL::Alloc()->initWithString($url);
		}

		/**
		 * @return URL
		 */
		public function getURL()
		{
			return $this->url;
		}

		/**
		 * @param string $name
		 * @param string $value
		 */
		public function setHeader($name, $value)
		{
			$this->headers[$name] = (string)$value;
		}

		/**
		 * @param string $name
		 * @return null|string
		 */
		public function getHeader($name)
		{
			return array_key_exists($name, $this->headers) ? $this->headers[$name] : null;
		}

		public function setContentType($content_type, $charset = 'UTF-8')
		{
			$this->setHeader(HeaderField::CONTENT_TYPE, $content_type.($charset ? '; charset='.$charset : ''));
		}

		public function getContentType()
		{
			$header_content_type = $this->getHeader(HeaderField::CONTENT_TYPE);
			list($content_type) = explode('; ', $header_content_type.'; ', 2);
			return $content_type;
		}

		public function setCookie($cookie)
		{
			$this->cookie = $cookie;
		}

		public function getCookie()
		{
			return $this->cookie;
		}

		public function setMethod($method)
		{
			$this->method = $method;
		}

		public function getMethod()
		{
			return $this->method;
		}

		public function setData($data, $content_type = null)
		{
			if ($content_type === null)
				$content_type = $this->getContentType();
			else
				$this->setContentType($content_type);

			switch($content_type)
			{
				case ContentType::APPLICATION_JSON:
					$this->data = json_encode($data);
					break;
				case ContentType::APPLICATION_FORM:
					$this->data = http_build_query($data);
					break;
				default:
					$this->data = is_string($data) ? $data : serialize($data);
					break;
			}
		}

		public function getData()
		{
			$content_type = $this->getContentType();
			$data = null;

			switch($content_type)
			{
				case ContentType::APPLICATION_JSON:
					$data = json_decode($this->data, true);
					break;
				case ContentType::APPLICATION_FORM:
					parse_str($this->data, $data);
					break;
				default:
					$this->data = is_string($data) ? $data : serialize($data);
					break;
			}
			return $data;
		}

		public function setRawData($data)
		{
			$this->data = $data;
		}

		public function getRawData()
		{
			return $this->data;
		}
	}

	class Request extends Message
	{
		protected $connection = null;

		/**
		 * @param Connection $connection
		 * @return Request
		 */
		public function initWithConnection(Connection $connection)
		{
			parent::init();

			$this->connection = $connection;

			// Get headers
			$this->headers = getallheaders();

			// Get method
			$this->method = strtoupper($_SERVER['REQUEST_METHOD']);

			// Get url components
			$this->url = URL::Object();

			// Get data
			$this->setRawData(file_get_contents('php://input'));

			return $this;
		}

		/**
		 * @return Response
		 * @throws \Exception
		 */
		public function send()
		{
			// Setup curl request
			$curl = curl_init();

			curl_setopt($curl, CURLOPT_URL, (string)$this->url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_VERBOSE, true);
			curl_setopt($curl, CURLOPT_HEADER, true);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->method);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $this->data);

			// Set content length
			$this->setContentLength(strlen($this->data));

			// Continue fix
			$this->setHeader(HeaderField::EXPECT, '');

			// Set headers
			if ($this->headers && count($this->headers))
			{
				$headers = array();
				foreach ($this->headers as $name => $value)
					array_push($headers, $name.': '.$value);

				curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			}

			// Send request
			$data = curl_exec($curl);

			// Get error if occurred
			if ($data === false)
			{
				$error_message = curl_error($curl);
				$error_code = curl_errno($curl);
				curl_close($curl);
				throw new \Exception($error_message, $error_code);
			}

			// Generate response
			$response_status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$response = Response::Alloc()->initWithOutgoingRequest($this, $response_status_code, $data);

			// Close connection
			curl_close($curl);

			// Return response
			return $response;
		}

		/**
		 * Set Authorization header
		 * @param Header\Authorization $auth
		 */
		public function setAuthorization(Header\Authorization $auth)
		{
			$this->setHeader(HeaderField::AUTHORIZATION, (string)$auth);
		}

		/**
		 * Get authorization header
		 * @return Header\Authorization|null
		 */
		public function getAuthorization()
		{
			$auth_header = $this->getHeader(HeaderField::AUTHORIZATION);
			return $auth_header ? Header\Authorization::Alloc()->initWithHeader($auth_header) : null;
		}

		/**
		 * @param int $length
		 */
		public function setContentLength($length)
		{
			$this->setHeader(HeaderField::CONTENT_LENGTH, $length);
		}

		/**
		 * @param string|array $content_type
		 */
		public function setAccept($content_type)
		{
			$content_types = implode(',', is_array($content_type) ? $content_type : func_get_args());
			$this->setHeader(HeaderField::ACCEPT, $content_type);
		}

		/**
		 * @return array|null
		 */
		public function getAccept()
		{
			$content_types = $this->getHeader(HeaderField::ACCEPT);
			return $content_types ? explode(',', $content_types) : null;
		}

		/**
		 * @param string $language
		 */
		public function setAcceptLanguage($language)
		{
			$this->setHeader(HeaderField::ACCEPT_LANGUAGE, $language);
		}

		/**
		 * @param string $default
		 * @return string|null
		 */
		public function getAcceptLanguage($default = null)
		{
			$lang = $this->getHeader(HeaderField::ACCEPT_LANGUAGE);
			return $lang ? substr($lang, 0, 2) : $default;
		}

		/**
		 * @param string $name
		 * @return Header\Range|array|null
		 */
		public function getRange($name = null)
		{
			$ranges = array();
			$range = $this->getHeader(HeaderField::RANGE);
			if ($range)
			{
				$list = explode(',', $range);
				foreach ($list as $value)
					if (($range = Header\Range::Alloc()->initWithString($value)))
						$ranges[$range->name] = $range;

				return ($name === null) ? $ranges : (isset($ranges[$name]) ? $ranges[$name] : null);
			}
			return null;
		}

		/**
		 * @param Header\Range $range
		 */
		public function setRange(Header\Range $range)
		{
			$this->setHeader(HeaderField::RANGE, (string)$range);
		}

		/**
		 * @param int $default_timestamp
		 * @return int
		 */
		public function getIfModifiedSince($default_timestamp = 0)
		{
			$mod_since = $this->getHeader(HeaderField::IF_MODIFIED_SINCE);
			return $mod_since ? strtotime($mod_since) : $default_timestamp;
		}

		/**
		 * @param int $timestamp
		 */
		public function setIfModifiedSince($timestamp)
		{
			$this->setHeader(HeaderField::IF_MODIFIED_SINCE, gmdate('D, d M Y H:i:s T', $timestamp));
		}
	}

	class Response extends Message
	{
		private $status_code = StatusCode::OK;

		/** @var Request  */
		private $_request = null;

		/**
		 * @param Request $request
		 * @return Response
		 */
		public function initWithIncomingRequest(Request $request)
		{
			parent::init();

			// Accepted content type
			if (($accept = $request->getAccept()))
				$this->setContentType(reset($accept));
			else
				$this->setContentType(ContentType::APPLICATION_JSON);

			// TODO - Parse set-cookie

			$this->_request = $request;

			return $this;
		}

		/**
		 * @param Request $request
		 * @param int $status_code
		 * @param string $data
		 * @return Request
		 */
		public function initWithOutgoingRequest(Request $request, $status_code, $data)
		{
			parent::init();

			$this->_request = $request;

			// Set status code
			$this->status_code = $status_code;

			// Set url
			$this->url = $request->url;

			// Set data
			if ($data)
			{
				// Separate headers from data
				list($header_data, $this->data) = explode("\r\n\r\n", $data, 2);

				// Parse headers
				$headers = explode("\r\n", $header_data);
				unset($headers[0]);
				foreach ($headers as $header)
				{
					list($name, $value) = explode(': ', $header, 2);
					$this->setHeader($name, $value);
				}
			}

			return $this;
		}

		/**
		 * Send Headers
		 */
		private function sendHeaders()
		{
			// Set response code
			header('HTTP/1.1 '.$this->status_code.' '.StatusCode::Description($this->status_code), true, $this->status_code);

			// Set headers
			foreach ($this->headers as $header => $value)
				header($header.': '.$value, true);

			// TODO Set cookie
		}

		/**
		 * Send Data
		 */
		public function send()
		{
			// Return data
			if ($this->data !== null)
			{
				switch($this->getContentType())
				{
					case ContentType::APPLICATION_FILE:
					case ContentType::IMAGE:
					case ContentType::IMAGE_GIF:
					case ContentType::IMAGE_PNG:
					case ContentType::IMAGE_JPG:
					case ContentType::IMAGE_JPEG:
						$this->sendFile($this->data);
						break;
					default:
						$this->sendHeaders();
						die($this->data);
						break;
				}
			}
			else
				$this->sendHeaders();

			die();
		}

		/**
		 * Send File
		 * @param string $path
		 */
		public function sendFile($path = null)
		{
			$path = $path ? $path : $this->data;
			$fd = (gettype($path) == 'resource') ? $path : (file_exists($path) ? @fopen($path, "r") : null);

			if ($fd)
			{
				// Get file info for mime extraction
				$finfo = new \finfo(FILEINFO_MIME);
				$buffer = fread($fd, 32);
				$mime = $finfo->buffer($buffer);

				//$meta = stream_get_meta_data($fd);

				// Get file stats
				$stat = fstat($fd);
				$last_modified = ($stat[9] > 0) ? $stat[9] : null; //filemtime($path);
				$file_size = $stat[7];

				// Check last modification
				if ($last_modified)
				{
					$mod_since = $this->_request ? $this->_request->getIfModifiedSince() : null;
					if ($mod_since && $mod_since >= $last_modified)
					{
						//header('Etag: '. md5(join(':', $stat)));
						header('HTTP/1.1 304 Not Modified', true, 304);
						exit;
					}
					$this->setHeader('Last-Modified', gmdate('D, d M Y H:i:s', $last_modified) . ' GMT');
				}

				// Set headers
				$this->setHeader(HeaderField::PRAGMA, 'public');
				$this->setHeader(HeaderField::CONTENT_LENGTH, $file_size);
				$this->setHeader(HeaderField::CONTENT_TYPE, $mime);

				// Send headers
				$this->sendHeaders();

				rewind($fd);
				fpassthru($fd);
				fclose($fd);
				exit;
			}
			else
			{
				header('HTTP/1.1 404 Not Found', true, 404);
				exit;
			}
		}

		/**
		 * Set Auth header
		 * @param Header\Authorization $auth
		 */
		public function setWWWAuthenticate(Header\Authorization $auth)
		{
			$params = null;

			if ($auth->type == Header\Authorization::TYPE_DIGEST)
			{
				$nonce = $auth->nonce ? $auth->nonce : md5(uniqid());
				$params = sprintf('realm="%s", nonce="%s", opaque="%s"', $auth->realm, $nonce, $auth->opaque);
			}
			else
				$params = sprintf('realm="%s"', $auth->realm);

			$this->setHeader(HeaderField::WWW_AUTHENTICATE, $auth->type.' '.$params);
		}

		/**
		 * Get Auth header
		 * @return Header\Authorization|null
		 */
		public function getWWWAuthenticate()
		{
			$auth_header = $this->getHeader(HeaderField::WWW_AUTHENTICATE);
			if ($auth_header)
			{
				$auth = Header\Authorization::Alloc()->initWithHeader($auth_header);
				$auth->method = strtoupper($this->method);
				return $auth;
			}
			return null;
		}

		/**
		 * @param int $code
		 */
		public function setStatusCode($code)
		{
			$this->status_code = (int)$code;
		}

		/**
		 * @return int
		 */
		public function getStatusCode()
		{
			return $this->status_code;
		}

		/**
		 * Set Content-Language header
		 * @param string $language
		 */
		public function setContentLanguage($language)
		{
			$this->setHeader(HeaderField::CONTENT_LANGUAGE, $language);
		}

		/**
		 * @param Header\Range|array $range
		 */
		public function setContentRange($range)
		{
			$range = is_array($range) ? implode(', ', $range) : (string)$range;
			$this->setHeader(HeaderField::CONTENT_RANGE, $range);
		}

		/**
		 * @return int
		 */
		public function getLastModified()
		{
			$last_modified = $this->getHeader(HeaderField::LAST_MODIFIED);
			return $last_modified ? strtotime($last_modified) : 0;
		}

		/**
		 * @param int $last_modified_timestamp
		 */
		public function setLastModified($last_modified_timestamp)
		{
			$this->setHeader(HeaderField::LAST_MODIFIED, gmdate('D, d M Y H:i:s T', $last_modified_timestamp));
		}
	}
}

namespace Shopacitor\HTTP\Header
{
	final class Authorization extends \Shopacitor\Object
	{
		const TYPE_BASIC = 'Basic';
		const TYPE_DIGEST = 'Digest';

		public $method = 'GET';
		public $type = self::TYPE_BASIC;
		public $username = null;
		public $password = null;
		public $realm = null;
		public $uri = null;
		public $nonce = null;
		public $opaque = null;
		public $qop = null;
		public $cnonce = null;
		public $nc = null;
		public $response = null;

		public function initWithCredentials($username, $password, $type = self::TYPE_BASIC)
		{
			parent::init();

			$this->type = $type;
			$this->username = $username;
			$this->password = $password;

			return $this;
		}

		public function initWithHeader($header_string)
		{
			parent::init();

			// Validate header
			$matches = array();
			if (!$header_string || !preg_match('/(Basic|Digest) (.*)/', $header_string, $matches))
				return $this;

			// Extract data
			list($n, $this->type, $params) = $matches;

			if ($this->type == self::TYPE_BASIC)
			{
				$credentials = base64_decode($params);
				list($this->username, $this->password) = explode(':', $credentials);
			}
			elseif ($this->type == self::TYPE_DIGEST && preg_match_all('@(realm|username|nonce|uri|nc|cnonce|qop|response|opaque)=[\'"]?([^\'",]+)@', $params, $matches))
			{
				$digest = array_combine($matches[1], $matches[2]);

				foreach ($digest as $name => $value)
					$this->$name = $value;
			}

			return $this;
		}

		public function validateCredentials($username, $password)
		{
			if ($this->type == self::TYPE_BASIC && $this->username === $username && $this->password === $password)
				return true;
			elseif ($this->type == self::TYPE_DIGEST && $this->response && (string)$this->username === (string)$username)
			{
				$this->password = $password;
				$response = $this->digestResponse();
				return ($this->response === $response);
			}
			else
				return false;
		}

		public function __toString()
		{
			if ($this->type == self::TYPE_BASIC && $this->username && $this->password)
			{
				$credentials = $this->username.':'.$this->password;
				return $this->type . ' ' . base64_encode($credentials);
			}
			elseif ($this->type == self::TYPE_DIGEST && $this->username && $this->password && $this->realm && $this->method && $this->uri && $this->nonce && $this->opaque)
			{
				if (!$this->nonce)
					$this->nonce = md5(uniqid().time());
				$this->response = $this->digestResponse();
				$credentials = sprintf('username="%s", realm="%s", nonce="%s", opaque="%s", uri="%s", response="%s"', $this->username, $this->realm, $this->nonce, $this->opaque, $this->uri, $this->response);
				return $this->type.' '.$credentials;
			}

			return '';
		}

		private function digestResponse()
		{
			$A1 = self::MD5ColonImplode($this->username, $this->realm, $this->password);
			$A2 = self::MD5ColonImplode(strtoupper($this->method), $this->uri);

			$response = ($this->qop && $this->nc && $this->cnonce) ?
			    self::MD5ColonImplode($A1, $this->nonce, $this->nc, $this->cnonce, $this->qop, $A2) :
			    self::MD5ColonImplode($A1, $this->nonce, $A2);

			return $response;
		}

		private static function MD5ColonImplode(/* string1, string2, stringN... */)
		{
			return md5(implode(':', func_get_args()));
		}
	}

	final class Range extends \Shopacitor\Object
	{
		/** @var string  */
		protected $name = 'items';

		/** @var int  */
		protected $first = 0;

		/** @var int  */
		protected $last = 99;

		/** @var int|null  */
		protected $length = null;

		/**
		 * @param string $name
		 * @param int $limit
		 * @param int $offset
		 * @return Range
		 */
		public static function RangeWithLimit($name, $limit = 100, $offset = 0)
		{
			return self::alloc()->initWithLimit($name, $limit, $offset);
		}

		/**
		 * @param string $name
		 * @param int $limit
		 * @param int $offset
		 * @return Range
		 */
		public function initWithLimit($name, $limit = 100, $offset = 0)
		{
			parent::Init();
			$this->name = $name;
			$this->setOffset($offset);
			$this->setLimit($limit);
			return $this;
		}

		/**
		 * @param $string
		 * @return Range|null
		 */
		public function initWithString($string)
		{
			parent::init();

			$matches = array();

			if (preg_match('/([a-z]+)\=([0-9]+)\-([0-9]+)/', $string, $matches) && count($matches) == 4)
				list($foo, $this->name, $this->first, $this->last) = $matches;
			elseif (preg_match('/([a-z]+) ([0-9]+)\-([0-9]+)\/([0-9]+)/', $string, $matches) && count($matches) == 5)
				list($foo, $this->name, $this->first, $this->last, $this->length) = $matches;

			if ($this->last < $this->first)
			{
				$this->dealloc();
				return null;
			}
			return $this;
		}

		public function __toString()
		{
			return $this->length !== null ?
			    $this->name.' '.$this->first.'-'.$this->last.'/'.$this->length :
			    $this->name.'='.$this->first.'-'.$this->last;
		}

		public function getLimit()
		{
			return $this->last - $this->first + 1;
		}

		public function setLimit($limit)
		{
			$this->last = $this->first + $limit - 1;
		}

		public function getOffset()
		{
			return $this->first;
		}

		public function setOffset($offset)
		{
			$this->last -= $this->first - $offset;
			$this->first = $offset;
		}

		public function setLength($length)
		{
			$this->length = $length;
			$this->last = $length > 0 ? min($this->last, $length-1) : 0;
		}

		public function getTotal()
		{
			return $this->length;
		}

		public function setTotal($total)
		{
			$this->setLength($total);
		}

		public function getName()
		{
			return $this->name;
		}

		public function setName($name)
		{
			$this->name = $name;
		}
	}
}