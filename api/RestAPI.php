<?php
/**
 *  Shopacitor Merchant API
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

namespace Shopacitor;

use \Shopacitor\URL;
use \Shopacitor\HTTP\Header;
use \Shopacitor\HTTP\Method;
use \Shopacitor\HTTP\Request;
use \Shopacitor\HTTP\Response;
use \Shopacitor\HTTP\Exception;
use \Shopacitor\HTTP\StatusCode;
use \Shopacitor\HTTP\ContentType;
use \Shopacitor\HTTP\HeaderField;

class Credentials extends Object
{
	public $username = null;
	public $password = null;

	public static function Credentials($username, $password)
	{
		$credentials = Credentials::Object();
		$credentials->username = $username;
		$credentials->password = $password;
		return $credentials;
	}
}

class Resource extends Object
{
	/** @var string */
	public $uri_pattern = null;

	/** @var string */
	public $method = null;

	/** @var callable */
	public $callback = null;

	/** @var Model */
	public $model = null;

	/** @var array */
	public $matches = null;
}

abstract class RestAPI extends HTTP\Connection
{
	/** @var HTTP\Request */
	protected $request = null;

	/** @var HTTP\Response */
	protected $response = null;

	/** @var array */
	protected $resources = array();

	/** @var string  */
	protected $powered_by = 'RestAPI';

	/** @var string  */
	protected $version = '1.0';

	/** @var  callable */
	protected $authentication_challenge_callback = null;

	protected function log($message)
	{
		static $count = 0;
		if ($this->response)
			$this->response->setHeader('X-Log-'.$count++, json_encode($message));
	}

	protected function handle($method, $uri_pattern, $model, $callback)
	{
		$resource = Resource::Object();
		$resource->method = $method;
		$resource->uri_pattern = $uri_pattern;
		$resource->model = $model;
		$resource->callback = $callback;
		array_push($this->resources, $resource);
	}

	/**
	 * @param string $uri_pattern
	 * @param Model $model
	 * @param callable $callback
	 */
	protected function handleGet($uri_pattern, $model, $callback)
	{
		$this->handle(Method::GET, $uri_pattern, $model, $callback);
	}

	/**
	 * @param string $uri_pattern
	 * @param Model $model
	 * @param callable $callback
	 */
	protected function handlePost($uri_pattern, $model, $callback)
	{
		$this->handle(Method::POST, $uri_pattern, $model, $callback);
	}

	/**
	 * @param string $uri_pattern
	 * @param Model $model
	 * @param callable $callback
	 */
	protected function handlePut($uri_pattern, $model, $callback)
	{
		$this->handle(Method::PUT, $uri_pattern, $model, $callback);
	}

	/**
	 * @param string $uri_pattern
	 * @param callable $callback
	 */
	protected function handleDelete($uri_pattern, $callback)
	{
		$this->handle(Method::DELETE, $uri_pattern, null, $callback);
	}

	/**
	 * @param Request $request
	 * @param Response $response
	 * @return boolean
	 */
	abstract protected function processRequestHandler(Request $request, Response $response);

	/**
	 * @param Header\Authorization $auth
	 * @return Credentials
	 */
	protected function handleAuthenticationChallenge(Header\Authorization $auth)
	{
		return null;
	}

	protected function validateAuthentication($username, $password)
	{
		$auth = $this->request->getAuthorization();

		// Validate authentication
		if ($auth && $auth->opaque && $auth->type == Header\Authorization::TYPE_DIGEST)
		{
			session_id($auth->opaque);
			session_start();

			$url = URL::Object();
			$auth->uri = $url->path;
			$auth->method = $this->request->getMethod();
			$auth->nonce = isset($_SESSION['auth-nonce']) ? $_SESSION['auth-nonce'] : md5(uniqid().time());

			unset($_SESSION['auth-nonce']);
			if ($auth->validateCredentials($username, $password))
				return;
		}

		if (!session_id())
			session_start();

		// Create auth request
		$auth = Header\Authorization::Object();
		$auth->type = Header\Authorization::TYPE_DIGEST;
		$auth->realm = $this->powered_by;
		$auth->opaque = session_id();
		$auth->nonce = $_SESSION['auth-nonce'] = md5(uniqid().time());

		// Send response with auth requirement
		$this->response->setWWWAuthenticate($auth);

		throw new Exception(StatusCode::UNAUTHORISED);
	}

	/**
	 * @param Request $request
	 * @return Response
	 * @throws Exception
	 */
	protected function handleRequest(Request $request)
	{
		$this->request = $request;
		$this->response = $response = Response::Alloc()->initWithIncomingRequest($this->request);

		try
		{
			$this->processRequestHandler($request, $response);

			/** @var \Shopacitor\Resource $found */
			$found = null;
			$allowed_methods = array();

			/** @var $resource \Shopacitor\Resource */
			foreach ($this->resources as $resource)
			{
				$pattern = '/^' . str_replace(array('%d', '%s'), array('([0-9]+)', '([0-9a-zA-Z\-_]+)'), preg_quote($resource->uri_pattern, '/')) . '$/';
				if (preg_match($pattern, $request->url->path, $resource->matches))
				{
					array_push($allowed_methods, $resource->method);
					if ($resource->method == $this->request->method)
						$found = $resource;
				}
			}

			if ($this->request->method == Method::OPTIONS && count($allowed_methods))
				$this->response->setHeader(HeaderField::ALLOW, implode(",", $allowed_methods));

			elseif ($found)
			{
				if (!is_callable($found->callback))
					throw new Exception(StatusCode::INTERNAL_SERVER_ERROR);

				// Get function params
				$params = $found->matches;
				array_shift($params);

				// Add model to params
				if ($found->model)
					array_push($params, $found->model);


				// Validate incoming data
				if ($found->model && in_array($found->method, array(Method::PUT, Method::POST)))
				{
					$data = $this->request->getData();
					if (!is_array($data) || !count($data))
						throw new Exception(StatusCode::BAD_REQUEST);

					$found->model->setProperties($data);
				}

				// Exec request handler
				$response_data = call_user_func_array($found->callback, $params);

				// Check response data
				if (!$response_data)
					throw new Exception(StatusCode::INTERNAL_SERVER_ERROR);


				// Set response data
				if (in_array($found->method, array(Method::GET, Method::POST)))
				{
					// Set new resource location header
					if ($found->model && $found->method == Method::POST)
					{
						$url = $request->getURL();
						$url->path .= $found->model->id;
						$response->setHeader(HeaderField::LOCATION, $url);
						$response->setStatusCode(StatusCode::CREATED);
					}
					else
						$response->setStatusCode(StatusCode::OK);

					// Set response data
					if ($found->model)
					{
						$response_data = $response_data->toArray();
						$response_type = ($found->model->model == 'Shopacitor\Model\Image') ? ContentType::IMAGE : ContentType::APPLICATION_JSON;
						$response->setData($response_data, $response_type);
					}
				}
			}
			elseif (count($allowed_methods))
				throw new Exception(StatusCode::METHOD_NOT_ALLOWED);

			else
				throw new Exception(StatusCode::NOT_IMPLEMENTED);
		}
		catch (\Exception $e)
		{
			$code = $e->getCode();

			$pathinfo = pathinfo($e->getFile());

			$error = Model\Error::Error($code, $e->getMessage(), $e->getLine(), $pathinfo['filename']);

			if (!StatusCode::Description($code))
				$code = StatusCode::INTERNAL_SERVER_ERROR;

			$this->response->setContentType(ContentType::APPLICATION_JSON);
			$this->response->setStatusCode($code);
			$this->response->setData($error->toArray());
		}

		$response->setHeader('X-Powered-By', $this->powered_by);
		$response->setHeader('X-Version', $this->version);

		return $response;
	}


	public function get($url, Model $model)
	{
		return $this->sendAPIRequest($url, Method::GET, $model);
	}

	public function put($url, Model $model)
	{
		return $this->sendAPIRequest($url, Method::PUT, $model);
	}

	public function post($url, Model $model)
	{
		return $this->sendAPIRequest($url, Method::POST, $model);
	}

	public function delete($url)
	{
		return $this->sendAPIRequest($url, Method::DELETE);
	}

	/**
	 * @param \Shopacitor\URL|string $url
	 * @param string $method
	 * @param \Shopacitor\Model|null $data_model
	 * @param string $content_type
	 * @return \Shopacitor\Model|boolean
	 * @throws \Exception
	 */
	protected function sendAPIRequest($url, $method = Method::GET, $data_model = null, $content_type = ContentType::APPLICATION_JSON)
	{
		if (!is_object($url))
			$url = URL::Alloc()->initWithString($url);

		// Set up request
		$request = Request::Object();
		$request->setURL($url);
		$request->setMethod($method);
		$request->setAccept($content_type);
		$request->setHeader('X-Powered-By', $this->powered_by);
		$request->setHeader('X-Version', $this->version);

		// Set data
		if ($data_model && in_array($method, array(Method::PUT, Method::POST)))
		{
			$data = is_subclass_of($data_model, '\Shopacitor\Model') ? $data_model->toArray() : $data_model;
			if (isset($data['model']) && $data['model'] == 'Shopacitor\Model\Image')
				$content_type = ContentType::IMAGE;

			$request->setData($data, $content_type);
		}

		// Send request
		$response = $this->sendRequest($request);

		// If Unauthorized
		if ($response->getStatusCode() == StatusCode::UNAUTHORISED)
		{
			$auth = $response->getWWWAuthenticate();
			if (!$auth)
				throw new \Exception('Invalid response header:WWW-WAuthenticate', StatusCode::EXPECTATION_FAILED);

			// Process Authentication Challenge
			$credentials = $this->authentication_challenge_callback ? call_user_func($this->authentication_challenge_callback, $auth) : $this->handleAuthenticationChallenge($auth);
			if (!$credentials)
				throw new Exception(StatusCode::UNAUTHORISED);

			$auth->username = $credentials->username;
			$auth->password = $credentials->password;

			$auth->uri = $url->path;
			$auth->method = $method;
			$request->setAuthorization($auth);
			$response = $this->sendRequest($request);
		}

		$status_code = $response->getStatusCode();

		// Validate empty get response
		if ($method == Method::GET && $status_code ==  StatusCode::NO_CONTENT)
			return true;

		// Validate PUT & DELETE Methods
		if (in_array($method, array(Method::PUT, Method::DELETE, Method::HEAD, Method::OPTIONS)) && $status_code == StatusCode::OK)
			return true;

		$data = $response->getData();

		// Get data
		if (!$data || !is_array($data))
			throw new \Exception('Invalid response data', StatusCode::EXPECTATION_FAILED);
		elseif (array_key_exists('model', $data) && $data['model'] == 'Shopacitor\Model\Error')
		{
			$error = Model\Error::ModelFromArray($data);
			throw new \Exception($error->message, $error->code);
		}

		$data_model->setProperties($data);
		return $data_model;
	}
}
