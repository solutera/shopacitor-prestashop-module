<?php
/**
 *  Shopacitor Merchant API
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

namespace Shopacitor;

function get_object_public_vars($object)
{
	return get_object_vars($object);
}

abstract class Object
{
	protected function __construct()
	{

	}
	
	final public function __destruct()
	{
		$this->dealloc();
	}

	/**
	 * Allocate object
	 * @return static
	 */
	public static function Alloc()
	{
		return new static(); //means that static sort of points to the class that is used
	}

	/**
	 * Deallocate object
	 */
	protected function dealloc()
	{

	}

	/**
	 * Initialize object
	 * @return static
	 */
	protected function init()
	{
		return $this;
	}

	/**
	 * Initialize object with properties
	 * @param array $vars
	 * @return static
	 */
	protected function initWithVars(array $vars)
	{
		$this->init();
		$this->setVars($vars);
		return $this;
	}

	/**
	 * Allocate & Init object
	 * @return static
	 */
	public static function Object()
	{
		return static::Alloc()->init();
	}

	/**
	 * Allocate & Init object with properties
	 * @param array $vars
	 * @return static
	 */
	public static function ObjectWithVars(array $vars)
	{
		return static::Alloc()->initWithVars($vars);
	}

	/**
	 * Set object properties
	 * @param array $vars
	 * @param null|array $filter
	 */
	public function setVars(array $vars, $filter = null)
	{
		if ($filter && !is_array($filter))
		{	
			$filter = func_get_args();
			array_shift($filter);
		}
		
		foreach ($vars as $name => $value) if ($filter == null || in_array($name, $filter))
		{
			$public_vars = get_object_public_vars($this);

			if (method_exists($this, ($method = 'set'.str_replace('_', '', $name))))
				$this->$method($value);
			elseif (isset($public_vars[$name]))
				$this->$name = $value;
		}
	}

	/**
	 * Get object properties
	 * @param null|array $filter
	 * @return array
	 * @throws \Exception
	 */
	public function getVars($filter = null)
	{
		$public_vars = get_object_public_vars($this);
		$filter = is_array($filter) ? $filter : func_get_args();
		$names = count($filter) ? $filter : array_keys($public_vars);
		$vars = array();

		foreach ($names as $name)
			$vars[$name] = isset($public_vars[$name]) ? $public_vars[$name] : $this->__get($name);

		return $vars;
	}

	public function __get($name)
	{
		$method = 'get'.str_replace('_', '', $name);
		if (!method_exists($this, $method))
			throw new \Exception('Getter method or property for "'.$name.'" on "'.get_class($this).'" class does not exists!');

		return $this->$method();
	}

	public function __set($name, $value)
	{
		$method = 'set'.str_replace('_', '', $name);
		if (!method_exists($this, $method))
			throw new \Exception('Setter method or property for "'.$name.'" on "'.get_class($this).'" class does not exists!');

		$this->$method($value);
	}
	
	public function __isset($name)
	{
		$method = 'get'.str_replace('_', '', $name);
		return method_exists($this, $method);
	}
}

class URL extends Object
{
	/** @var string  */
	public $scheme = 'http';

	/** @var string  */
	public $host = 'localhost';

	/** @var string  */
	public $path = '/';

	/** @var array  */
	public $params = array();

	/**
	 * @param string $url_string
	 * @return $this
	 */
	public function initWithString($url_string)
	{
		parent::init();
		$this->setString($url_string);
		return $this;
	}

	/**
	 * @param string $scheme
	 * @param string $host
	 * @param string $path
	 * @param array $params
	 * @return $this
	 */
	public function initWithComponents($scheme = 'http', $host = 'domain.com', $path = '/', $params = array())
	{
		parent::init();
		$this->scheme = $scheme;
		$this->host = $host;
		$this->path = $path;
		$this->params = $params;
		return $this;
	}

	/**
	 * @return URL
	 */
	public function init()
	{
		parent::init();
		$scheme = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
		list($path, $foo) = explode('?', $_SERVER['REQUEST_URI'].'?', 2);
		return $this->initWithComponents($scheme, $_SERVER['HTTP_HOST'], $path, $_GET);
	}

	/**
	 * @param string $url_string
	 */
	public function setString($url_string)
	{
		$components = parse_url($url_string);

		foreach (array('scheme', 'host', 'path') as $name)
			$this->$name = isset($components[$name]) ? $components[$name] : null;

		if (isset($components['query']))
			parse_str($components['query'], $this->params);
	}

	public function __toString()
	{
		$string = '';
		if ($this->scheme)
			$string .= $this->scheme.':';

		if ($this->host)
			$string .= '//'.$this->host;

		$string .= $this->path;

		if ($this->params && count($this->params))
			$string .= '?'.http_build_query($this->params);

		return $string;
	}

	/**
	 * @param string $name
	 * @param string $value
	 */
	public function setParam($name, $value)
	{
		$this->params[$name] = $value;
	}

	/**
	 * @param string $name
	 * @return string|null
	 */
	public function getParam($name)
	{
		return isset($this->params[$name]) ? $this->params[$name] : null;
	}
}

