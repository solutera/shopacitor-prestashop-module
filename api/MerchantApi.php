<?php
/**
 *  Shopacitor Merchant API
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

error_reporting(E_ALL | E_STRICT);

require_once('Object.php');
require_once('Models.php');
require_once('HTML.php');
require_once('RestAPI.php');

use \Shopacitor\Model;
use \Shopacitor\RestAPI;
use \Shopacitor\Credentials;
use \Shopacitor\HTTP\Request;
use \Shopacitor\HTTP\Response;
use \Shopacitor\HTTP\Exception;
use \Shopacitor\HTTP\StatusCode;
use \Shopacitor\HTTP\Header\Range;
use \Shopacitor\HTTP\Header\Authorization;

abstract class ShopacitorMerchantApi extends RestAPI
{
	const SERVER_API_URL = 'http://api.shopacitor.com/';

	protected $powered_by = 'Shopacitor-Merchant-Api';
	protected $version = '1.0';
	public $shop_id = 0;
	public $api_key = '';

	public static function ApiWithCredentials($shop_id, $api_key)
	{
		return self::Alloc()->initWithCredentials($shop_id, $api_key);
	}

	public function initWithCredentials($shop_id, $api_key)
	{
		$this->shop_id = $shop_id;
		$this->api_key = $api_key;
		return $this->init();
	}

	protected function handleAuthenticationChallenge(Authorization $auth)
	{
		return Credentials::Credentials($this->shop_id, $this->api_key);
	}

	protected function processRequestHandler(Request $request, Response $response)
	{
		$request->url->path = $request->url->getParam('resource');

		$info = $this->getInfo(Model\Shop::Model());

		if ($info->mode == Model\Shop::MODE_NONE && !in_array($request->url->path, array('info', 'logo')))
			throw new \Exception('Service Unavailable', StatusCode::SERVICE_UNAVAILABLE);

		if ($info->mode == Model\Shop::MODE_TEST)
			$this->response->setHeader('X-Mode', 'dev');

		// Register handlers
		$this->handleGet('info', 			Model\Shop::Model(), 		array($this, 'getInfo'));
		$this->handleGet('logo',			Model\Image::Model(), 		array($this, 'getLogo'));
		$this->handleGet('language/',			Model\Container::Model(), 	array($this, 'getLanguages'));
		$this->handleGet('shipping-location/',		Model\Container::Model(), 	array($this, 'getShippingLocations'));
		$this->handleGet('category/%d', 		Model\Category::Model(), 	array($this, '_getCategory'));

		$this->handleGet('product/', 			Model\Container::Model(), 	array($this, '_getProducts'));

		$this->handleGet('product/%d', 			Model\Product::Model(), 	array($this, '_getProduct'));
		$this->handleGet('product/%d/combination/', 	Model\Container::Model(), 	array($this, '_getProductCombinations'));
		$this->handleGet('product/%d/image/%d', 	Model\Image::Model(), 		array($this, '_getProductImage'));

		$this->handleGet('product/%d/image/%d', 	Model\Image::Model(), 		array($this, '_getProductImage'));
		$this->handleGet('product/%d/image/%d', 	Model\Image::Model(), 		array($this, '_getProductImage'));

		$this->handlePost('customer/', 			Model\Customer::Model(), 	array($this, 'registerCustomer'));
		$this->handleGet('customer/%d', 		Model\Customer::Model(), 	array($this, 'getCustomer'));

		$this->handleGet('cart/%d',			Model\Cart::Model(), 		array($this, '_getCart'));
		$this->handleDelete('cart/%d', 							array($this, 'clearCart'));

		$this->handlePost('cart/%d/product/', 		Model\CartProduct::Model(), 	array($this, 'cartAddProduct'));
		$this->handlePut('cart/%d/product/%s', 		Model\CartProduct::Model(), 	array($this, 'cartUpdateProduct'));
		$this->handleDelete('cart/%d/product/%s', 					array($this, 'cartRemoveProduct'));

		$this->handlePut('cart/%d/address/shipping', 	Model\Address::Model(), 	array($this, 'cartSetShippingAddress'));
		$this->handleGet('cart/%d/carrier/', 		Model\Container::Model(), 	array($this, '_cartGetAvailableCarriers'));
		$this->handlePost('cart/%d/carrier', 		null, 				array($this, '_cartSelectCarrier'));

		$this->handleGet('cart/%d/payment_method/',	Model\Container::Model(),	array($this, '_cartGetPaymentMethods'));
		$this->handleGet('cart/%d/payment_method/%s',	Model\PaymentMethod::Model(),	array($this, 'cartGetPaymentMethod'));

		$this->handlePost('order/',			Model\Order::Model(), 		array($this, 'createOrder'));
		$this->handlePut('order/%d',			Model\Order::Model(), 		array($this, '_updateOrder'));

		$this->handleGet('page/',			Model\Container::Model(),	array($this, '_getPages'));
		$this->handleGet('page/%d',			Model\Page::Model(),		array($this, '_getPage'));
	}

	protected function _getProducts(Model\Container $products)
	{
		$lang = $this->request->getAcceptLanguage();
		$mod_since = $this->request->getIfModifiedSince();
		$range = $this->request->getRange('products');
		if (!$range)
			$range = Range::RangeWithLimit('products', 10, 0);

		$range->total = $this->getProducts($products, $lang, $mod_since, $range->getOffset(), $range->getLimit());

		$this->response->setContentRange($range);

		return $products;
	}

	protected function _getProduct($product_id, Model\Product $product)
	{
		$lang = $this->request->getAcceptLanguage();
		return $this->getProduct($product_id, $lang, $product);
	}

	protected function _getProductCombinations($product_id, Model\Container $combinations)
	{
		$lang = $this->request->getAcceptLanguage();
		return $this->getProductCombinations($product_id, $lang, $combinations);
	}

	protected function _getProductImage($product_id, $image_id, Model\Image $image)
	{
		$lang = $this->request->getAcceptLanguage();

		// TODO set language
		//$this->response->setContentLanguage($ps_language->iso_code);

		$size = $this->request->url->getParam('size');
		$image->path = $this->getProductImage($product_id, $image_id, $lang, $size);

		return $image;
	}

	protected function _getCategory($category_id, Model\Category $category)
	{
		$lang = $this->request->getAcceptLanguage();
		$range = $this->request->getRange('products');
		if (!$range)
			$range = Range::RangeWithLimit('products', 10, 0);

		$range->total = $this->getCategory($category_id, $lang, $range->getOffset(), $range->getLimit(), $category);
		$this->response->setContentRange($range);

		return $category;
	}

	protected function _getCart($cart_id, Model\Cart $cart)
	{
		$lang = $this->request->getAcceptLanguage();
		return $this->getCart($cart_id, $lang, $cart);
	}

	protected function _cartGetAvailableCarriers($cart_id, Model\Container $carriers)
	{
		$lang = $this->request->getAcceptLanguage();
		return $this->cartGetAvailableCarriers($cart_id, $lang, $carriers);
	}

	protected function _cartSelectCarrier($cart_id)
	{
		$carrier_id = $this->request->url->getParam('selected');
		return $this->cartSelectCarrier($cart_id, $carrier_id);
	}

	protected function _cartGetPaymentMethods($cart_id, Model\Container $payment_methods)
	{
		// Currently supported just PayPal
		$paypal = Model\PayPalPaymentMethod::Model();
		$payment_methods->addModel($paypal);
		return $payment_methods;
	}

	protected function cartGetPaymentMethod($cart_id, $method_identifier, Model\PaymentMethod $method = null)
	{
		if ($method_identifier == Model\PayPalPaymentMethod::PAYPAL)
			$method = $this->cartGetPaypalPaymentMethod($cart_id, Model\PayPalPaymentMethod::Model());
		else
			throw new \Exception('Payment method not found', StatusCode::NOT_FOUND);

		return $method;
	}

	protected function _updateOrder($order_id, Model\Order $order)
	{
		$this->validateAuthentication($this->shop_id, $this->api_key);
		return $this->updateOrder($order_id, $order);
	}

	protected function _getPages(Model\Container $pages)
	{
		$lang = $this->request->getAcceptLanguage();
		return $this->getPages($lang, $pages);
	}

	protected function _getPage($page_id, Model\Page $page)
	{
		$lang = $this->request->getAcceptLanguage();
		return $this->getPage($page_id, $lang, $page);
	}


	/**
	 * @return Credentials Customer username & password
	 * @throws Exception
	 */
	protected function getCustomerCredentials()
	{
		$auth = $this->request->getAuthorization();
		if (!$auth || !$auth->username || !$auth->password)
		{
			$auth = Authorization::Object();
			$auth->type = Authorization::TYPE_BASIC;
			$auth->realm = $this->powered_by;
			$this->response->setWWWAuthenticate($auth);
			throw new Exception(StatusCode::UNAUTHORISED);
		}
		return Credentials::Credentials($auth->username, $auth->password);
	}

	/**
	 * Get cart token to identify cart user
	 * @return null|string
	 */
	protected function getCartToken()
	{
		return $this->request->url->getParam('token');
	}

        /**
	 * Get shop information from server
	 * @return Model\Shop
	 */
	public function getShop()
	{
		return $this->get(self::SERVER_API_URL.'shop/'.$this->shop_id, Model\Shop::Model());
	}

	/**
	 * Register new shop
	 * @param Model\Shop $shop - shop information (required: domain, email, api_url, logo_url)
	 * @return Model\Shop
	 */
	public function addShop(Model\Shop $shop)
	{
		return $this->post(self::SERVER_API_URL.'shop/', $shop);
	}

	/**
	 * Update shop data
	 * @param Model\Shop $shop - shop information (required: domain, email, api_url, logo_url)
	 * @return Model\Shop
	 */
	public function updateShop(Model\Shop $shop)
	{
		return $this->put(self::SERVER_API_URL.'shop/'.$this->shop_id, $shop);
	}

        /**
	 * Add product - inform server about new product
	 * @param Model\Product $product
	 * @return Model\Product
	 */
	public function addProduct(Model\Product $product)
	{
		return $this->post(self::SERVER_API_URL.'shop/'.$this->shop_id.'/product/', $product);
	}

        /**
	 * Update product
	 * @param Model\Product $product
	 * @return boolean
	 */
	public function updateProduct(Model\Product $product)
	{
		$this->put(self::SERVER_API_URL.'shop/'.$this->shop_id.'/product/'.$product->id, $product);
	}

        /**
	 * Delete product
	 * @param int $product_id
	 * @return true or false on error
	 */
	public function deleteProduct($product_id)
	{
		return $this->delete(self::SERVER_API_URL.'shop/'.$this->shop_id.'/product/'.$product_id);
	}


	/**
	 * Get current shop info
	 * @param Model\Shop $shop
	 * @return Model\Shop
	 */
	abstract public function getInfo(Model\Shop $shop);

	/**
	 * Get current shop logo
	 * @param Model\Image $logo
	 * @return Model\Image
	 */
	abstract public function getLogo(Model\Image $logo);

	/**
	 * Get available languages
	 * @param Model\Container $container
	 * @return Model\Container - Containing Model\Language models
	 */
	abstract public function getLanguages(Model\Container $container);

	/**
	 * Get available shipping location
	 * @param Model\Container $container
	 * @return Model\Container = Containing Model\ShippingLocation models
	 */
	abstract public function getShippingLocations(Model\Container $container);

	/**
	 * Get product textual information that where updated before given timestamp
	 * @param Model\Container $container Fill this container with Product Models
	 * @param string $lang Language iso code (two chars)
	 * @param int $modified_since Timestamp
	 * @param int $offset Offset
	 * @param int $limit Limit
	 * @return int - Total products without limit
	 */
	abstract public function getProducts(Model\Container $container, $lang, $modified_since, $offset, $limit);

	/**
	 * Get full product information
	 * @param int $product_id
	 * @param string $lang Language iso code (two chars)
	 * @param Model\Product $product Model to use for return
	 * @return Model\Product
	 */
	abstract public function getProduct($product_id, $lang, Model\Product $product);

	/**
	 * Get product combinations
	 * @param int $product_id
	 * @param string $lang Language iso code (two chars)
	 * @param Model\Container $combinations
	 * @return Model\Container - Containing Model\Product models
	 */
	abstract public function getProductCombinations($product_id, $lang, Model\Container $combinations);

	/**
	 * Get product image
	 * @param int $product_id
	 * @param int $image_id
	 * @param string $lang Language iso code (two chars)
	 * @param int $size Recommended minimum image size (height|width)
	 * @param Model\Image $image Model to return
	 * @return string Local path to image
	 */
	abstract public function getProductImage($product_id, $image_id, $lang, $size);

	/**
	 * Get products and subcategories for given category
	 * @param int $category_id
	 * @param string $lang Language iso code (two chars)
	 * @param  int $products_offset
	 * @param  int $products_limit
	 * @param  Model\Category $category
	 * @return int Total product in category
	 */
	abstract public function getCategory($category_id, $lang, $products_offset, $products_limit, Model\Category $category);

	/**
	 * @param Model\Customer $customer
	 * @return Model\Customer
	 */
	abstract public function registerCustomer(Model\Customer $customer);

	/**
	 * Get customer details - Use getCustomerCredentials to authenticate customer
	 * @param int $customer_id
	 * @param Model\Customer $customer
	 * @return Model\Customer
	 */
	abstract public function getCustomer($customer_id, Model\Customer $customer);

	/**
	 * Get shopping cart details.
	 * @param int $cart_id
	 * @param string $lang Language iso code (two chars)
	 * @param Model\Cart $cart
	 * @return Model\Cart
	 */
	abstract public function getCart($cart_id, $lang, Model\Cart $cart);

	/**
	 * Clear cart
	 * @param int $cart_id
	 * @return bool
	 */
	abstract public function clearCart($cart_id);

	/**
	 * @param int $cart_id
	 * @param string $cart_product_id
	 * @return bool
	 */
	abstract public function cartRemoveProduct($cart_id, $cart_product_id);

	/**
	 * Add product cart
	 * @param int $cart_id
	 * @param Model\CartProduct $cartProduct
	 * @return Model\CartProduct
	 */
	abstract public function cartAddProduct($cart_id, Model\CartProduct $cartProduct);

	/**
	 * Add product cart
	 * @param int $cart_id
	 * @param string $cart_product_id
	 * @param Model\CartProduct $cart_product
	 * @return Model\CartProduct
	 */
	abstract public function cartUpdateProduct($cart_id, $cart_product_id, Model\CartProduct $cart_product);

	/**
	 * @param int $cart_id
	 * @param Model\Address $address
	 * @return bool
	 */
	abstract public function cartSetShippingAddress($cart_id, Model\Address $address);

	/**
	 * @param int $cart_id
	 * @param string $lang Language iso code (two chars)
	 * @param Model\Container $carriers
	 * @return Model\Container
	 */
	abstract public function cartGetAvailableCarriers($cart_id, $lang, Model\Container $carriers);

	/**
	 * @param int $cart_id
	 * @param int $carrier_id Selected carrier
	 * @return bool
	 */
	abstract public function cartSelectCarrier($cart_id, $carrier_id);

	/**
	 * @param int $cart_id
	 * @param Model\PayPalPaymentMethod $paypal
	 * @return Model\PayPalPaymentMethod
	 */
	abstract public function cartGetPaypalPaymentMethod($cart_id, Model\PayPalPaymentMethod $paypal);

	/**
	 * Create new order
	 * @param Model\Order $order
	 * @return Model\Order
	 */
	abstract public function createOrder(Model\Order $order);

	/**
	 * Update order status
	 * @param int $order_id
	 * @param Model\Order $order
	 * @return Model\Order
	 */
	abstract public function updateOrder($order_id, Model\Order $order);

	/**
	 * @param string $lang Language iso code (two chars)
	 * @param Model\Container $pages
	 * @return Model\Container
	 */
	abstract public function getPages($lang, Model\Container $pages);

	/**
	 * @param $page_id
	 * @param string $lang Language iso code (two chars)
	 * @param Model\Page $page
	 * @return Model\Page
	 */
	abstract public function getPage($page_id, $lang, Model\Page $page);
}
