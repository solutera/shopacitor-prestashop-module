<?php
/**
 *  Shopacitor Merchant API
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

namespace Shopacitor
{
	class Model
	{
		public $id = null;
		public $model = null;

		public static function Model()
		{
			return new static();
		}

		protected function __construct()
		{
			$this->model = get_class($this);
		}

		/**
		 * @param array $vars
		 * @return static
		 */
		public static function ModelFromArray(array $vars)
		{
			$model = null;

			// Validate model
			if (isset($vars['model']) && is_string($vars['model']) && class_exists($vars['model']))
				$model = $vars['model']::Model();
			else
				$model = static::Model();

			$model->setProperties($vars);

			return $model;
		}

		/**
		 * @param array $vars
		 */
		public function setProperties(array $vars)
		{
			foreach ($vars as $name => $value)
				if (property_exists($this, $name))
					$this->$name = (is_array($value) && isset($value['model'])) ? Model::ModelFromArray($value) : $value;
		}

		/**
		 * @return array
		 */
		public function toArray()
		{
			$array = get_object_vars($this);
			foreach ($array as $k => $v)
				if ($v instanceof Model)
					$array[$k] = $v->toArray();
			return array_filter($array);
		}
	}
}

namespace Shopacitor\Model {

	use \Shopacitor\Model as Model;

	class Container extends Model implements \Iterator
	{
		private $_models = array();
		private $_position = 0;

		public function addModel(Model $model)
		{
			array_push($this->_models, $model);
		}

		public function setModel($name, $model)
		{
			$this->_models[$name] = $model;
		}

		public function toArray()
		{
			$array = array();
			foreach ($this->_models as $k => $model)
				$array[$k] = $model->toArray();
			return array_filter($array);
		}

		public function rewind()
		{
			$this->_position = 0;
		}

		public function current()
		{
			return $this->_models[$this->_position];
		}

		public function key()
		{
			return $this->_position;
		}

		public function next()
		{
			$this->_position++;
		}

		public function valid()
		{
			return isset($this->_models[$this->_position]);
		}
	}

	class Error extends Model
	{
		public $code = 0;
		public $line = 0;
		public $file = null;
		public $message = null;

		public static function Error($code, $message = null, $line = 0, $file = null)
		{
			$error = Error::Model();
			$error->code = $code;
			$error->line = $line;
			$error->file = $file;
			$error->message = $message;
			return $error;
		}

		public function __toString()
		{
			return $this->message;
		}
	}

	class Image extends Model
	{
		public $path = null;

		public static function ImageWithPath($path)
		{
			$image = Image::Model();
			$image->path = $path;
			return $image;
		}

		public function __toString()
		{
			return $this->path;
		}

		public function toArray()
		{
			return $this->path;
		}
	}

	class Shop extends Model
	{
		const STATUS_CONFIRMED = 'confirmed';
		const STATUS_PROCESSING = 'processing';
		const STATUS_WAITING = 'waiting';
		const STATUS_REJECTED = 'rejected';
		const STATUS_UNKNOWN = 'unknown';

		const MODE_ACTIVE = 'active';
		const MODE_TEST = 'test';
		const MODE_NONE = 'none';

		public $id = 0;
		public $name = null;
		public $domain = null;
		public $email = null;
		public $api_url = null;
		public $api_key = null;
		public $status = null;
		public $status_msg = null;
		public $mode = null;
	}

	class Language extends Model
	{
		public $iso_639 = null;
		public $iso_3166 = null;
		public $name = null;

		public static function LanguageWithIETFTag($tag, $name = null)
		{
			$language = self::Model();
			list($language->iso_639, $language->iso_3166) = explode('-', $tag . '-', 3);
			$language->name = $name;
			return $language;
		}
	}

	class ShippingLocation extends Model
	{
		public $country_iso = null;
		public $state_iso = null;
		public $country = null;
		public $state = null;

		public static function LocationWithISO($country_iso, $state_iso = null, $country = null, $state = null)
		{
			$model = self::Model();
			$model->country_iso = $country_iso;
			$model->state_iso = $state_iso;
			$model->country = $country;
			$model->state = $state;
			return $model;
		}
	}


	class ProductImage extends Model
	{
		public $id = 0;
		public $description = null;
		public $is_cover = false;
	}

	class ProductFeature extends Model
	{
		public $name = null;
		public $value = null;
	}

	class ProductField extends Model
	{
		const TYPE_NUMBER = 'number';
		const TYPE_TEXT = 'text';
		const TYPE_IMAGE = 'image';
		const TYPE_FILE = 'file';

		public $id = 0;
		public $name = null;
		public $type = null;
		public $description = null;
		public $required = false;
	}

	class ProductAttributeValue extends Model
	{
		public $id = 0;
		public $name = null;
	}

	class ProductAttributeGroup extends Model
	{
		public $id = 0;
		public $name = null;
		public $values = array();

		public function addValue(ProductAttributeValue $value)
		{
			array_push($this->values, $value);
		}
	}

	class Product extends Model
	{
		public $id = 0;
		public $cover_id = 0;
		public $category_id = 0;
		public $name = '';

		public $currency = null;
		public $price = null;
		public $old_price = null;
		public $tax_rate = null;

		public $is_featured = false;

		public $reference = null;
		public $manufacturer = null;
		public $short_description = null;
		public $description = null;
		public $ean13 = null;
		public $tags = null;

		public $created = 0;
		public $updated = 0;

		public $url = null;
		public $min_qty = 0;
		public $available = false;
		public $available_msg = null;

		public $features = array();
		public $images = array();
		public $attributes = array();
		public $options = array();
		public $fields = array();

		public function ProductWithSummary($id, $name, $currency, $price, $cover_id = 0, $old_price = 0)
		{
			$product = self::Model();
			$product->id = $id;
			$product->name = $name;
			$product->currency = $currency;
			$product->price = $price;
			$product->old_price = $old_price;
			$product->cover_id = $cover_id;
		}

		public function getSummary()
		{
			return (object)array
			(
				'id' => $this->id,
				'name' => $this->name,
				'currency' => $this->currency,
				'price' => $this->price,
				'old_price' => $this->old_price,
				'cover_id' => $this->cover_id
			);
		}

		public function addImage(ProductImage $image)
		{
			array_push($this->images, $image);
		}

		public function addFeature(ProductFeature $feature)
		{
			array_push($this->features, $feature);
		}

		public function addAttributeGroup(ProductAttributeGroup $group)
		{
			array_push($this->attributes, $group);
		}

		public function addField(ProductField $field)
		{
			array_push($this->fields, $field);
		}
	}

	class Combination extends Product
	{
	}

	class Category extends Model
	{
		public $id = 0;
		public $name = '';
		public $subcategories = null;
		public $products = null;

		protected function __construct()
		{
			parent::__construct();
			$this->subcategories = Container::Model();
			$this->products = Container::Model();
		}

		public function addSubCategory(Category $category)
		{
			$this->subcategories->addModel($category);
		}

		public function addProduct(Product $product)
		{
			$this->products->addModel($product);
		}
	}

	class CartProduct extends Model
	{
		public $id = 0;
		public $product_id = 0;
		public $combination_id = 0;
		public $cover_id = 0;
		public $name = null;
		public $details = null;
		public $quantity = 0;
		public $price = 0.0;
	}

	class Cart extends Model
	{
		public $id = 0;
		public $currency = null;
		public $total_products = 0.0;
		public $total_shipping = 0.0;
		public $products_tax = 0.0;
		public $shipping_tax = 0.0;
		public $total_tax = 0.0;
		public $total = 0.0;
		public $token = null;
		public $url = null;

		/** @var Container */
		public $products = array();
		/** @var Container */
		protected $address = array();

		public $carrier = null;

		protected function __construct()
		{
			parent::__construct();
			$this->products = Container::Model();
			$this->address = Container::Model();
		}

		public function addProduct(CartProduct $product)
		{
			$this->products->addModel($product);
		}

		public function setShippingAddress(Address $address)
		{
			$this->address->setModel('shipping', $address);
		}
	}


	class Customer extends Model
	{
		public $id = 0;
		public $email = null;
		public $firstname = null;
		public $lastname = null;
		public $company = null;
		public $password = null;
	}

	class Carrier extends Model
	{
		public $id = 0;
		public $name = null;
		public $details = null;
		public $price = 0.0;
	}

	class Address extends Model
	{
		public $id = 0;
		public $email = null;
		public $firstname = null;
		public $lastname = null;
		public $company = null;
		public $vat = null;
		public $dni = null;
		public $country = null;
		public $state = null;
		public $line1 = null;
		public $line2 = null;
		public $zip = null;
		public $city = null;
		public $phone1 = null;
		public $phone2 = null;
		public $other = null;
	}

	class PaymentMethod extends Model
	{
		const PAYPAL = 'paypal';

		public $id = null;
		public $name = null;
		public $details = null;
	}

	class PayPalPaymentMethod extends PaymentMethod
	{
		public $merchant_email = null;

		protected function __construct()
		{
			parent::__construct();
			$this->id = PaymentMethod::PAYPAL;
			$this->name = 'PayPal';
		}
	}

	class Order extends Model
	{
		const PAYMENT_STATUS_PENDING =		'pending';
		const PAYMENT_STATUS_COMPLETED =	'completed';
		const PAYMENT_STATUS_CANCELED = 	'canceled';
		const PAYMENT_STATUS_REFUNDED = 	'refunded';
		const PAYMENT_STATUS_ERROR =		'error';

		public $id = 0;
		public $cart_id = 0;
		public $cart_token = null;
		public $carrier_id = 0;
		public $payment_transaction_id = null;
		public $payment_method_id = null;
		public $payment_status = null;
		public $currency = null;
		public $total_amount = null;
		public $user_message = null;
		public $custom_data = null;
	}

	class Page extends Model
	{
		const TYPE_ABOUT = 'about';
		const TYPE_TERMS = 'terms';
		const TYPE_OTHER = 'other';
		const TYPE_SHIPPING = 'shipping';

		public $id = 0;
		public $title = null;
		public $type = self::TYPE_OTHER;
		public $text = null;
	}
}