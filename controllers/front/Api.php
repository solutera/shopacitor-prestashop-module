<?php
/**
 *  Shopacitor Module for Prestashop
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

class ShopacitorApiModuleFrontController extends ModuleFrontController
{
	protected $content_only = true;

	public function initContent()
	{
		parent::initContent();
		$api = PSMerchantApi::ApiWithCredentials(Configuration::get('SHOPACITOR_SHOP_ID'), Configuration::get('SHOPACITOR_SHOP_API_KEY'));
		$api->invoke();
		die();
	}
}
