<?php
/**
 *  Shopacitor Module for Prestashop
 *
 *  2015 Shopacitor
 *
 *  @author    Domas Labokas <domas@solutera.lt>
 *  @copyright Copyright (c) 2015, JSC Solutera
 *  @version   1.0
 *  @license   End User License Agreement (EULA)
 *  @link      http://www.solutera.lt
 */

header('Access-Control-Allow-Origin: http://api-docs.shopacitor.com');
header('Access-Control-Allow-Headers: If-Modified-Since,Range,Content-Type,Authorization,*');
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,OPTIONS');
header('Access-Control-Allow-Credentials: true');

require_once(dirname(__FILE__).'/../../config/config.inc.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once(dirname(__FILE__).'/classes/PSMerchantApi.php');

$controller = new FrontController();
$controller->init();

$api = PSMerchantApi::ApiWithCredentials(Configuration::get('SHOPACITOR_SHOP_ID'), Configuration::get('SHOPACITOR_SHOP_API_KEY'));
$api->invoke();
